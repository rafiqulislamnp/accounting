﻿using KGERP.Data.Models;
using KGERP.Service.Implementation.Accounting;
using KGERP.Service.ServiceModel;
using KGERP.Utility;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;


namespace KGERP.Service.Implementation

{
    public class AccountingService
    {
        private readonly ERPEntities _db;
        string _urlInfo = "";
        public AccountingService(ERPEntities db)
        {
            _db = db;
            _urlInfo = GetErpUrlInfo();
        }

        public string GetErpUrlInfo()
        {
            int pi;


            return _db.UrlInfoes.Where(x => x.UrlType == 1).Select(x => x.Url).FirstOrDefault();
        }
        public async Task<long> AccountingJournalPush(DateTime journalDate, int CompanyFK, int drHeadID, long? crHeadID, decimal amount, string title, string description, int journalType)
        {
            long result = -1;
            VMJournalSlave vMJournalSlave = new VMJournalSlave
            {
                JournalType = journalType,
                Title = title,
                Narration = description,
                CompanyFK = CompanyFK,
                Date = journalDate,
                IsSubmit = true
            };

            vMJournalSlave.DataListSlave = new List<VMJournalSlave>();
            vMJournalSlave.DataListSlave.Add(new VMJournalSlave
            {
                Particular = description,
                Debit = Convert.ToDouble(amount),
                Credit = 0,
                Accounting_HeadFK = drHeadID
            });
            vMJournalSlave.DataListSlave.Add(new VMJournalSlave
            {
                Particular = description,
                Debit = 0,
                Credit = Convert.ToDouble(amount),
                Accounting_HeadFK = Convert.ToInt32(crHeadID)
            });

            var resultData = await AccountingJournalMasterPush(vMJournalSlave);
            return resultData.VoucherId;
        }

        public async Task<long> AccountingInventoryPush(DateTime journalDate, int CompanyFK, int adjustHeadDr, int adjustHeadCr, decimal adjustValue, string title, string description, int journalType)
        {
            long result = -1;
            VMJournalSlave vMJournalSlave = new VMJournalSlave
            {
                JournalType = journalType,
                Title = title,
                Narration = description,
                CompanyFK = CompanyFK,
                Date = journalDate,

                IsSubmit = true
            };


            vMJournalSlave.DataListSlave = new List<VMJournalSlave>();

            vMJournalSlave.DataListSlave.Add(new VMJournalSlave
            {
                Particular = description,
                Debit = 0,
                Credit = Convert.ToDouble(adjustValue),
                Accounting_HeadFK = adjustHeadCr
            });
            vMJournalSlave.DataListSlave.Add(new VMJournalSlave
            {
                Particular = description,
                Debit = Convert.ToDouble(adjustValue),
                Credit = 0,
                Accounting_HeadFK = adjustHeadDr
            });
            var resultData = await AccountingJournalMasterPush(vMJournalSlave);
            return resultData.VoucherId;
        }

        public async Task<Voucher> AccountingJournalMasterPush(VMJournalSlave vmJournalSlave)
        {
            long result = -1;
            Accounting_CostCenter _CostCenter = new Accounting_CostCenter();
            if (vmJournalSlave.CompanyFK == 7 || vmJournalSlave.CompanyFK == 9)
            {
                if (vmJournalSlave.Accounting_CostCenterFK != null)
                {
                    _CostCenter.CostCenterId = (int)vmJournalSlave.Accounting_CostCenterFK;
                }
                else
                {
                    _CostCenter = _db.Accounting_CostCenter.Where(x => x.CompanyId == vmJournalSlave.CompanyFK).FirstOrDefault();
                }
            }
            else
            {
                _CostCenter = _db.Accounting_CostCenter.Where(x => x.CompanyId == vmJournalSlave.CompanyFK).FirstOrDefault();
            }
            string voucherNo = GetNewVoucherNo(vmJournalSlave.JournalType, vmJournalSlave.CompanyFK.Value, vmJournalSlave.Date.Value);
            Voucher voucher = new Voucher
            {
                VoucherTypeId = vmJournalSlave.JournalType,
                VoucherNo = voucherNo,
                Accounting_CostCenterFk = _CostCenter.CostCenterId,
                VoucherStatus = "A",
                VoucherDate = vmJournalSlave.Date,
                Narration = vmJournalSlave.Title + " " + vmJournalSlave.Narration,
                ChqDate = vmJournalSlave.ChqDate,
                ChqName = vmJournalSlave.ChqName,
                ChqNo = vmJournalSlave.ChqNo,
                CompanyId = vmJournalSlave.CompanyFK,
                CreatedBy = System.Web.HttpContext.Current.User.Identity.Name,
                CreateDate = DateTime.Now,
                IsActive = true,
                IsSubmit = vmJournalSlave.IsSubmit,
                IsIntegrated = true
            };

            using (var scope = _db.Database.BeginTransaction())
            {
                try
                {
                    _db.Vouchers.Add(voucher);
                    _db.SaveChanges();
                    List<VoucherDetail> voucherDetailList = new List<VoucherDetail>();
                    voucherDetailList = vmJournalSlave.DataListSlave.Select(x => new VoucherDetail
                    {
                        Particular = x.Particular,
                        DebitAmount = Convert.ToDouble(x.Debit),
                        CreditAmount = Convert.ToDouble(x.Credit),
                        AccountHeadId = x.Accounting_HeadFK,
                        IsActive = true,
                        VoucherId = voucher.VoucherId,
                        TransactionDate = voucher.VoucherDate,
                        IsVirtual = x.IsVirtual
                    }).ToList();

                    _db.VoucherDetails.AddRange(voucherDetailList);  
                    scope.Commit();
                    if (_db.SaveChanges() > 0)
                    {
                        result = voucher.VoucherId;                        
                    }
                    if (result > 0)
                    {                        
                        var v = _db.Vouchers.Find(voucher.VoucherId);
                        v.TotalAmount = voucherDetailList
                            .Where(x => x.IsActive == true && x.IsVirtual == false)
                            .Select(x => x.CreditAmount).DefaultIfEmpty(0).Sum();
                        _db.SaveChanges();
                    }
                    return voucher;
                }
                catch (Exception ex)
                {
                    scope.Rollback();
                    return voucher;
                }
            }
        }

        public string GetNewVoucherNo(int voucherTypeId, int companyId, DateTime voucherDate)
        {
            VoucherType voucherType = _db.VoucherTypes.Where(x => x.VoucherTypeId == voucherTypeId).FirstOrDefault();
            string voucherNo = string.Empty;
            int vouchersCount = _db.Vouchers.Where(x => x.VoucherTypeId == voucherTypeId && x.CompanyId == companyId
            && x.VoucherDate.Value.Month == voucherDate.Month
            && x.VoucherDate.Value.Year == voucherDate.Year).Count();

            vouchersCount++;
            voucherNo = voucherType.Code + "-" + vouchersCount.ToString().PadLeft(4, '0');
            //if (vouchersCount  == 0)
            //{               
            //    voucherNo = voucherType.Code + "-" + "000001";                
            //}
            //else
            //{

            //    //Voucher voucher = context.Vouchers.Where(x => x.VoucherTypeId == voucherTypeId && x.CompanyId == companyId).OrderByDescending(x => x.VoucherNo).FirstOrDefault();
            //    //voucherNo = GenerateVoucherNo(voucher.VoucherNo);
            //}           
            return voucherNo;
        }

        public string GetVoucherNo(int voucherTypeId, int companyId)
        {
            string voucherNo = string.Empty;
            var vouchers = _db.Vouchers.Where(x => x.VoucherTypeId == voucherTypeId && x.CompanyId == companyId);

            if (!vouchers.Any())
            {
                VoucherType voucherType = _db.VoucherTypes.Where(x => x.VoucherTypeId == voucherTypeId).FirstOrDefault();
                voucherNo = voucherType.Code + "-" + "0001";
                return voucherNo;
            }
            Voucher voucher = _db.Vouchers.Where(x => x.VoucherTypeId == voucherTypeId && x.CompanyId == companyId).OrderByDescending(x => x.VoucherNo).FirstOrDefault();
            voucherNo = GenerateVoucherNo(voucher.VoucherNo);
            return voucherNo;
        }

        private string GenerateVoucherNo(string lastVoucherNo)
        {
            string prefix = lastVoucherNo.Substring(0, 4);
            int code = Convert.ToInt32(lastVoucherNo.Substring(4, 6));
            int newCode = code + 1;
            return prefix + newCode.ToString().PadLeft(6, '0');
        }
        public async Task<VMJournalSlave> GetCompaniesDetails(int companyId)
        {
            VMJournalSlave vmJournalSlave = new VMJournalSlave();
            vmJournalSlave = await Task.Run(() => (from t1 in _db.Companies.Where(x => x.IsActive && x.CompanyId == companyId)

                                                   select new VMJournalSlave
                                                   {
                                                       CompanyFK = t1.CompanyId,
                                                       CompanyName = t1.Name
                                                   }).FirstOrDefault());


            return vmJournalSlave;
        }

        public async Task<VMJournalSlave> GetVoucherDetails(int companyId, int voucherId)
        {
            VMJournalSlave vmJournalSlave = new VMJournalSlave();
            vmJournalSlave = await Task.Run(() => (from t1 in _db.Vouchers.Where(x => x.IsActive && x.VoucherId == voucherId && x.CompanyId == companyId)
                                                   join t4 in _db.VoucherTypes on t1.VoucherTypeId equals t4.VoucherTypeId
                                                   join t2 in _db.Companies on t1.CompanyId equals t2.CompanyId
                                                   join t3 in _db.Accounting_CostCenter on t1.Accounting_CostCenterFk equals t3.CostCenterId
                                                   //  join t5 in _db.HeadGLs on t1.VirtualHeadId equals t5.Id

                                                   select new VMJournalSlave
                                                   {
                                                       VoucherId = t1.VoucherId,
                                                       Accounting_CostCenterName = t3.Name,
                                                       VoucherNo = t1.VoucherNo,
                                                       Date = t1.VoucherDate,
                                                       Narration = t1.Narration,
                                                       CompanyFK = t1.CompanyId,
                                                       Status = t1.VoucherStatus,
                                                       ChqDate = t1.ChqDate,
                                                       ChqName = t1.ChqName,
                                                       ChqNo = t1.ChqNo,
                                                       Accounting_CostCenterFK = t1.Accounting_CostCenterFk,
                                                       Accounting_BankOrCashId = t1.VirtualHeadId,
                                                       //BankOrCashNane = "[" + t5.AccCode + "] " + t5.AccName,
                                                       CompanyName = t2.Name,
                                                       IsSubmit = t1.IsSubmit
                                                   }).FirstOrDefault());

            vmJournalSlave.DataListDetails = await Task.Run(() => (from t1 in _db.VoucherDetails.Where(x => x.IsActive && x.VoucherId == voucherId && !x.IsVirtual)
                                                                   join t2 in _db.HeadGLs on t1.AccountHeadId equals t2.Id
                                                                   select new VMJournalSlave
                                                                   {
                                                                       VoucherDetailId = t1.VoucherDetailId,
                                                                       AccountingHeadName = t2.AccName,
                                                                       Code = t2.AccCode,
                                                                       Credit = t1.CreditAmount,
                                                                       Debit = t1.DebitAmount,
                                                                       Particular = t1.Particular
                                                                   }).OrderByDescending(x => x.VoucherDetailId).AsEnumerable());
            if (vmJournalSlave.DataListDetails.Any())
            {
                vmJournalSlave.Particular = vmJournalSlave.DataListDetails.OrderByDescending(x => x.VoucherDetailId).Select(x => x.Particular).FirstOrDefault();
            }
            return vmJournalSlave;
        }

        public async Task<VMJournalSlave> GetStockVoucherDetails(int companyId, int voucherId)
        {
            VMJournalSlave vmJournalSlave = new VMJournalSlave();
            vmJournalSlave = await Task.Run(() => (from t1 in _db.Vouchers.Where(x => x.IsActive && x.VoucherId == voucherId && x.CompanyId == companyId)
                                                   join t4 in _db.VoucherTypes on t1.VoucherTypeId equals t4.VoucherTypeId
                                                   join t2 in _db.Companies on t1.CompanyId equals t2.CompanyId
                                                   join t3 in _db.Accounting_CostCenter on t1.Accounting_CostCenterFk equals t3.CostCenterId

                                                   select new VMJournalSlave
                                                   {
                                                       VoucherId = t1.VoucherId,
                                                       Accounting_CostCenterName = t3.Name,
                                                       VoucherNo = t1.VoucherNo,
                                                       Date = t1.VoucherDate,
                                                       Narration = t1.Narration,
                                                       CompanyFK = t1.CompanyId,
                                                       Status = t1.VoucherStatus,
                                                       ChqDate = t1.ChqDate,
                                                       ChqName = t1.ChqName,
                                                       ChqNo = t1.ChqNo,
                                                       Accounting_CostCenterFK = t1.Accounting_CostCenterFk,
                                                       Accounting_BankOrCashId = t1.VirtualHeadId,
                                                       IsSubmit = t1.IsSubmit

                                                   }).FirstOrDefault());

            vmJournalSlave.DataListDetails = await Task.Run(() => (from t1 in _db.VoucherDetails.Where(x => x.IsActive && x.VoucherId == voucherId && !x.IsVirtual)
                                                                   join t2 in _db.HeadGLs on t1.AccountHeadId equals t2.Id
                                                                   select new VMJournalSlave
                                                                   {
                                                                       VoucherDetailId = t1.VoucherDetailId,
                                                                       AccountingHeadName = t2.AccName,
                                                                       Code = t2.AccCode,
                                                                       Credit = t1.CreditAmount,
                                                                       Debit = t1.DebitAmount,
                                                                       Particular = t1.Particular
                                                                   }).OrderByDescending(x => x.VoucherDetailId).AsEnumerable());
            return vmJournalSlave;
        }
        public async Task<long> VoucherAdd(VMJournalSlave vmJournalSlave)
        {
            long result = -1;
            //GetVoucherNo

            Voucher voucher = new Voucher
            {
                Narration = vmJournalSlave.Narration,
                VoucherNo = vmJournalSlave.VoucherNo,
                VoucherStatus = vmJournalSlave.Status,
                VoucherTypeId = vmJournalSlave.VoucherTypeId,
                ChqDate = vmJournalSlave.ChqDate,
                VirtualHeadId = vmJournalSlave.Accounting_BankOrCashId,
                ChqNo = vmJournalSlave.ChqNo,

                Accounting_CostCenterFk = vmJournalSlave.Accounting_CostCenterFK,
                ChqName = vmJournalSlave.ChqName,
                VoucherDate = vmJournalSlave.Date,
                CompanyId = vmJournalSlave.CompanyFK,
                CreatedBy = System.Web.HttpContext.Current.User.Identity.Name,
                CreateDate = DateTime.Now,
                IsActive = true,
                IsStock = vmJournalSlave.IsStock
            };
            _db.Vouchers.Add(voucher);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = voucher.VoucherId;
            }
            return result;
        }
        public async Task<long> VoucherDetailAdd(VMJournalSlave vmJournalSlave)
        {
            long result = -1;
            if ((vmJournalSlave.Accounting_HeadFK > 0) && (vmJournalSlave.Debit > 0 || vmJournalSlave.Credit > 0))
            {
                VoucherDetail voucherDetail = new VoucherDetail
                {
                    AccountHeadId = vmJournalSlave.Accounting_HeadFK,
                    CreditAmount = vmJournalSlave.Credit,
                    DebitAmount = vmJournalSlave.Debit,
                    Particular = vmJournalSlave.Particular,
                    TransactionDate = DateTime.Now,
                    VoucherId = vmJournalSlave.VoucherId,

                    IsActive = true
                };
                _db.VoucherDetails.Add(voucherDetail);
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = voucherDetail.VoucherDetailId;
                }
            }


            return result;
        }



        public async Task<long> VoucherDetailsEdit(VMJournalSlave vmJournalSlave)
        {
            long result = -1;
            VoucherDetail model = await _db.VoucherDetails.FindAsync(vmJournalSlave.VoucherDetailId);

            model.AccountHeadId = vmJournalSlave.Accounting_HeadFK;
            model.CreditAmount = vmJournalSlave.Credit;
            model.DebitAmount = vmJournalSlave.Debit;
            model.Particular = vmJournalSlave.Particular;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = model.VoucherDetailId;
            }

            return result;
        }
        public async Task<long> VoucherDelete(VoucherModel voucherModel)
        {
            long result = -1;
            Voucher model = await _db.Vouchers.FindAsync(voucherModel.VoucherId);

            model.IsActive = false;

            List<VoucherDetail> VoucherDetailList = _db.VoucherDetails.Where(x => x.VoucherId == voucherModel.VoucherId).ToList();
            VoucherDetailList.ForEach(x => x.IsActive = false);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = model.VoucherId;
            }

            return result;
        }
        public async Task<long> VoucherUndoSubmit(VoucherModel voucherModel)
        {
            long result = -1;
            Voucher model = await _db.Vouchers.FindAsync(voucherModel.VoucherId);
            model.IsSubmit = false;
            model.VoucherStatus = null;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = model.VoucherId;
            }
            return result;
        }
        public async Task<long> VoucherDetailsDelete(long voucherDetailId)
        {
            long result = -1;
            VoucherDetail model = await _db.VoucherDetails.FindAsync(voucherDetailId);

            model.IsActive = false;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = model.VoucherDetailId;
            }

            return result;
        }
        public List<object> CostCenterDropDownList(int companyId)
        {
            var List = new List<object>();
            _db.Accounting_CostCenter
        .Where(x => x.IsActive && x.CompanyId == companyId).Select(x => x).ToList()
        .ForEach(x => List.Add(new
        {
            Value = x.CostCenterId,
            Text = x.Name
        }));
            return List;

        }

        public List<object> VoucherTypesCashAndBankDropDownList()
        {
            var List = new List<object>();
            _db.VoucherTypes
        .Where(x => x.IsActive && x.VoucherTypeId <= (int)JournalEnum.CashReceive).Select(x => x).ToList()
        .ForEach(x => List.Add(new
        {
            Value = x.VoucherTypeId,
            Text = x.Name
        }));
            return List;

        }

        public List<object> VoucherTypesDownList(int companyId)
        {
            var List = new List<object>();
            _db.VoucherTypes
        .Where(x => x.IsActive && x.CompanyId == companyId).Select(x => x).ToList()
        .ForEach(x => List.Add(new
        {
            Value = x.VoucherTypeId,
            Text = x.Name
        }));
            return List;

        }
        public List<object> VoucherTypesJournalVoucherDropDownList()
        {
            var List = new List<object>();
            _db.VoucherTypes
        .Where(x => x.IsActive && x.VoucherTypeId == (int)JournalEnum.JournalVoucer).Select(x => x).ToList()
        .ForEach(x => List.Add(new
        {
            Value = x.VoucherTypeId,
            Text = x.Name
        }));
            return List;

        }
        public List<object> SeedCashAndBankDropDownList(int companyId)
        {
            var List = new List<object>();
            var v = (from t1 in _db.Head4
                     join t2 in _db.Head3 on t1.ParentId equals t2.Id
                     where t2.AccCode == "1301" && t1.CompanyId == companyId
                     select new
                     {
                         Value = t1.Id,
                         Text = t1.AccCode + " -" + t1.AccName
                     }).ToList();

            foreach (var item in v)
            {
                List.Add(new { Text = item.Text, Value = item.Value });
            }

            return List;

        }

        public List<object> GCCLLCFactoryExpanceHeadGLList(int companyId)
        {
            var List = new List<object>();
            var v = (from t1 in _db.HeadGLs
                     join t2 in _db.Head5 on t1.ParentId equals t2.Id
                     join t3 in _db.Head4 on t2.ParentId equals t3.Id
                     join t4 in _db.Head3 on t3.ParentId equals t4.Id

                     where (t4.AccCode == "4103" || t4.AccCode == "4104" || t4.AccCode == "4105"
                     || t4.AccCode == "4106" || t2.AccCode == "1301002002" || t3.AccCode == "2401013")
                     && t1.CompanyId == companyId
                     select new
                     {
                         Value = t1.Id,
                         Text = t4.AccCode + " -" + t4.AccName + " " + t1.AccCode + " -" + t1.AccName
                     }).ToList();
            foreach (var item in v)
            {
                List.Add(new { Text = item.Text, Value = item.Value });
            }
            return List;

        }


        public List<object> GCCLOtherIncomeHeadGLList(int companyId)
        {
            var List = new List<object>();
            var v = (from t1 in _db.HeadGLs
                     join t2 in _db.Head5 on t1.ParentId equals t2.Id

                     where t2.AccCode == "3101002001"
                     && t1.CompanyId == companyId
                     select new
                     {
                         Value = t1.Id,
                         Text = t2.AccCode + " -" + t2.AccName + " " + t1.AccCode + " -" + t1.AccName
                     }).ToList();
            foreach (var item in v)
            {
                List.Add(new { Text = item.Text, Value = item.Value });
            }
            return List;

        }



        public List<object> GCCLCashAndBankDropDownList(int companyId)
        {
            var List = new List<object>();
            var v = (from t1 in _db.Head5
                     join t2 in _db.Head4 on t1.ParentId equals t2.Id
                     where (t2.AccCode == "1301001" || t1.AccCode == "1301002002") && t1.CompanyId == companyId
                     select new
                     {
                         Value = t1.Id,
                         Text = t1.AccCode + " -" + t1.AccName
                     }).ToList();

            foreach (var item in v)
            {
                List.Add(new { Text = item.Text, Value = item.Value });
            }

            return List;

        }
        public List<object> CashAndBankDropDownList(int companyId)
        {
            var List = new List<object>();
            var v = (from t1 in _db.Head5
                     join t2 in _db.Head4 on t1.ParentId equals t2.Id
                     where (t2.AccCode == "1301001") && t1.CompanyId == companyId
                     select new
                     {
                         Value = t1.Id,
                         Text = t1.AccCode + " -" + t1.AccName
                     }).ToList();

            foreach (var item in v)
            {
                List.Add(new { Text = item.Text, Value = item.Value });
            }
            return List;
        }
        public Company GetCompanyById(int companyId)
        {
            var company = _db.Companies.Where(x => x.CompanyId == companyId).FirstOrDefault();
            return company;
        }
        public List<object> KPLCashAndBankDropDownList(int companyId)
        {
            var List = new List<object>();
            var v = (from t1 in _db.Head5
                     join t2 in _db.Head4 on t1.ParentId equals t2.Id
                     where (t2.AccCode == "1301001") && t1.CompanyId == companyId
                     select new
                     {
                         Value = t1.Id,
                         Text = t1.AccCode + " -" + t1.AccName
                     }).ToList();

            foreach (var item in v)
            {
                List.Add(new { Text = item.Text, Value = item.Value });
            }

            return List;

        }



        public List<object> StockDropDownList(int companyId)
        {
            var List = new List<object>();

            if (companyId == (int)CompanyName.NaturalFishFarmingLimited)
            {
                var v = (from t1 in _db.Head5
                         where t1.AccCode == "1301004001" && t1.CompanyId == companyId
                         select new
                         {
                             Value = t1.Id,
                             Text = t1.AccCode + " -" + t1.AccName
                         }).ToList();

                foreach (var item in v)
                {
                    List.Add(new { Text = item.Text, Value = item.Value });
                }
            }
            if (companyId == (int)CompanyName.KrishibidBazaarLimited)
            {
                var v = (from t1 in _db.Head5
                         where t1.AccCode == "1301005001" && t1.CompanyId == companyId
                         select new
                         {
                             Value = t1.Id,
                             Text = t1.AccCode + " -" + t1.AccName
                         }).ToList();

                foreach (var item in v)
                {
                    List.Add(new { Text = item.Text, Value = item.Value });
                }
            }
            if (companyId == (int)CompanyName.OrganicPoultryLimited || companyId == (int)CompanyName.SonaliOrganicDairyLimited)
            {
                var v = (from t1 in _db.Head5
                         join t2 in _db.Head4 on t1.ParentId equals t2.Id
                         where t2.AccCode == "1301004" && t1.CompanyId == companyId
                         select new
                         {
                             Value = t1.Id,
                             Text = t1.AccCode + " -" + t1.AccName
                         }).ToList();

                foreach (var item in v)
                {
                    List.Add(new { Text = item.Text, Value = item.Value });
                }
            }
            if (companyId == (int)CompanyName.KrishibidPrintingAndPublicationLimited)
            {
                var v = (from t1 in _db.Head5
                         join t2 in _db.Head4 on t1.ParentId equals t2.Id
                         where t2.AccCode == "1305001" && t1.CompanyId == companyId
                         select new
                         {
                             Value = t1.Id,
                             Text = t1.AccCode + " -" + t1.AccName
                         }).ToList();

                foreach (var item in v)
                {
                    List.Add(new { Text = item.Text, Value = item.Value });
                }
            }

            if (companyId == (int)CompanyName.KrishibidFoodAndBeverageLimited)
            {
                var v = (from t1 in _db.Head5
                         join t2 in _db.Head4 on t1.ParentId equals t2.Id
                         join t3 in _db.Head3 on t2.ParentId equals t3.Id

                         where t3.AccCode == "1305" && t1.CompanyId == companyId
                         select new
                         {
                             Value = t1.Id,
                             Text = t1.AccCode + " -" + t1.AccName
                         }).ToList();

                foreach (var item in v)
                {
                    List.Add(new { Text = item.Text, Value = item.Value });
                }
            }
            if (companyId == (int)CompanyName.KrishibidPackagingLimited)
            {
                var v = (
                    from t1 in _db.Head5
                    join t2 in _db.Head4 on t1.ParentId equals t2.Id
                    where t2.AccCode == "1301005" && t1.CompanyId == companyId
                    select new
                    {
                        Value = t1.Id,
                        Text = t1.AccCode + " -" + t1.AccName
                    }).ToList();

                foreach (var item in v)
                {
                    List.Add(new { Text = item.Text, Value = item.Value });
                }
            }
            if (companyId == (int)CompanyName.KrishibidFisheriesLimited)
            {
                var v = (
                    from t1 in _db.Head5
                    where t1.AccCode == "1301005001" && t1.CompanyId == companyId
                    select new
                    {
                        Value = t1.Id,
                        Text = t1.AccCode + " -" + t1.AccName
                    }).ToList();

                foreach (var item in v)
                {
                    List.Add(new { Text = item.Text, Value = item.Value });
                }
            }
            if (companyId == (int)CompanyName.KrishibidPoultryLimited)
            {
                var v = (
                    from t1 in _db.Head5
                    where t1.AccCode == "1301005001" && t1.CompanyId == companyId
                    select new
                    {
                        Value = t1.Id,
                        Text = t1.AccCode + " -" + t1.AccName
                    }).ToList();

                foreach (var item in v)
                {
                    List.Add(new { Text = item.Text, Value = item.Value });
                }
            }
            if (companyId == (int)CompanyName.KrishibidTradingLimited)
            {
                var v = (
                    from t1 in _db.Head5
                    where t1.AccCode == "1305001001" && t1.CompanyId == companyId
                    select new
                    {
                        Value = t1.Id,
                        Text = t1.AccCode + " -" + t1.AccName
                    }).ToList();

                foreach (var item in v)
                {
                    List.Add(new { Text = item.Text, Value = item.Value });
                }
            }
            if (companyId == (int)CompanyName.KrishibidSafeFood)
            {
                var v = (
                    from t1 in _db.Head5

                    where t1.ParentId == 50611881 && t1.CompanyId == companyId
                    select new
                    {
                        Value = t1.Id,
                        Text = t1.AccCode + " -" + t1.AccName
                    }).ToList();

                foreach (var item in v)
                {
                    List.Add(new { Text = item.Text, Value = item.Value });
                }
            }

            return List;
        }
        public async Task<long> FishariseAutoInsertStockVoucherDetails(int companyId, int voucherId)
        {
            long result = -1;

            var voucher = await _db.Vouchers.FindAsync(voucherId);

            var fromDate = voucher.VoucherDate.Value.AddDays(0 - voucher.VoucherDate.Value.Day).AddDays(-10);
            var todate = voucher.VoucherDate.Value.AddDays(0 - voucher.VoucherDate.Value.Day);
            var priviousStock = (from t1 in _db.VoucherDetails
                                 join t2 in _db.Vouchers on t1.VoucherId equals t2.VoucherId
                                 join headGL in _db.HeadGLs on t1.AccountHeadId equals headGL.Id
                                 join head5 in _db.Head5 on headGL.ParentId equals head5.Id
                                 join head4 in _db.Head4 on head5.ParentId equals head4.Id
                                 where t2.CompanyId == companyId && head5.AccCode == "1301005001"
                                 && t1.IsActive && t2.IsActive && !t1.IsVirtual &&
                                 t2.Accounting_CostCenterFk == voucher.Accounting_CostCenterFk &&
                                     t2.VoucherDate >= fromDate
                                     && t2.VoucherDate <= todate

                                 select new
                                 {
                                     AccountHeadId = t1.AccountHeadId,
                                     DebitAmount = t1.DebitAmount
                                 }).ToList();
            var fromExistDate = voucher.VoucherDate.Value.AddDays(-10);
            var currentExistStock = (from t1 in _db.VoucherDetails
                                     join t2 in _db.Vouchers on t1.VoucherId equals t2.VoucherId
                                     join headGL in _db.HeadGLs on t1.AccountHeadId equals headGL.Id
                                     join head5 in _db.Head5 on headGL.ParentId equals head5.Id
                                     join head4 in _db.Head4 on head5.ParentId equals head4.Id
                                     where t2.CompanyId == companyId && head5.AccCode == "1301005001"
                                     && t1.IsActive && t2.IsActive && !t1.IsVirtual &&

    t2.VoucherDate >= fromExistDate

                                                  && t2.VoucherDate <= voucher.VoucherDate.Value
                                                  && t1.VoucherId != voucher.VoucherId

                                     select new
                                     {
                                         AccountHeadId = t1.AccountHeadId,
                                         DebitAmount = t1.DebitAmount
                                     }).AsEnumerable();

            var currentStock = (from t1 in _db.VoucherDetails
                                where t1.VoucherId == voucher.VoucherId && t1.IsActive
                                select new
                                {
                                    DebitAmount = t1.DebitAmount,
                                    AccountHeadId = t1.AccountHeadId

                                }).ToList();

            List<VoucherDetail> voucherDetailList = new List<VoucherDetail>();
            if (!currentExistStock.Any())
            {
                foreach (var item in priviousStock.Where(x => x.DebitAmount > 0))
                {
                    VoucherDetail voucherDetail = new VoucherDetail
                    {
                        VoucherId = voucher.VoucherId,

                        AccountHeadId = item.AccountHeadId,
                        CreditAmount = item.DebitAmount,
                        DebitAmount = 0,
                        TransactionDate = DateTime.Now,
                        IsActive = true,
                        IsVirtual = true


                    };
                    voucherDetailList.Add(voucherDetail);
                    VoucherDetail voucherDetail1 = new VoucherDetail
                    {
                        VoucherId = voucher.VoucherId,

                        AccountHeadId = 50611909,
                        CreditAmount = 0,
                        DebitAmount = item.DebitAmount,
                        TransactionDate = DateTime.Now,
                        IsActive = true,
                        IsVirtual = true


                    };
                    voucherDetailList.Add(voucherDetail1);

                }
            }

            foreach (var item in currentStock)
            {


                VoucherDetail voucherDetail1 = new VoucherDetail
                {
                    VoucherId = voucher.VoucherId,

                    AccountHeadId = 50611909,
                    CreditAmount = item.DebitAmount,
                    DebitAmount = 0,
                    TransactionDate = DateTime.Now,
                    IsActive = true
                };
                voucherDetailList.Add(voucherDetail1);

            }

            _db.VoucherDetails.AddRange(voucherDetailList);

            if (await _db.SaveChangesAsync() > 0)
            {
                result = voucher.VoucherId;
            }

            return result;

        }

        public async Task<long> PoultryAutoInsertStockVoucherDetails(int companyId, int voucherId)
        {
            long result = -1;

            var voucher = await _db.Vouchers.FindAsync(voucherId);

            var fromDate = voucher.VoucherDate.Value.AddDays(0 - voucher.VoucherDate.Value.Day).AddDays(-10);
            var todate = voucher.VoucherDate.Value.AddDays(0 - voucher.VoucherDate.Value.Day);
            var priviousStock = (from t1 in _db.VoucherDetails
                                 join t2 in _db.Vouchers on t1.VoucherId equals t2.VoucherId
                                 join headGL in _db.HeadGLs on t1.AccountHeadId equals headGL.Id
                                 join head5 in _db.Head5 on headGL.ParentId equals head5.Id
                                 join head4 in _db.Head4 on head5.ParentId equals head4.Id
                                 where t2.CompanyId == companyId && head5.AccCode == "1301005001"
                                 && t1.IsActive && t2.IsActive && !t1.IsVirtual &&
                                 t2.Accounting_CostCenterFk == voucher.Accounting_CostCenterFk &&

                                     t2.VoucherDate >= fromDate
                                     && t2.VoucherDate <= todate

                                 select new
                                 {
                                     AccountHeadId = t1.AccountHeadId,
                                     DebitAmount = t1.DebitAmount
                                 }).ToList();
            var fromExistDate = voucher.VoucherDate.Value.AddDays(-10);
            var currentExistStock = (from t1 in _db.VoucherDetails
                                     join t2 in _db.Vouchers on t1.VoucherId equals t2.VoucherId
                                     join headGL in _db.HeadGLs on t1.AccountHeadId equals headGL.Id
                                     join head5 in _db.Head5 on headGL.ParentId equals head5.Id
                                     where t2.CompanyId == companyId && head5.AccCode == "1301005001"
                                     && t1.IsActive && t2.IsActive && !t1.IsVirtual &&

    t2.VoucherDate >= fromExistDate

                                                  && t2.VoucherDate <= voucher.VoucherDate.Value
                                                  && t1.VoucherId != voucher.VoucherId

                                     select new
                                     {
                                         AccountHeadId = t1.AccountHeadId,
                                         DebitAmount = t1.DebitAmount
                                     }).AsEnumerable();

            var currentStock = (from t1 in _db.VoucherDetails
                                where t1.VoucherId == voucher.VoucherId && t1.IsActive
                                select new
                                {
                                    DebitAmount = t1.DebitAmount,
                                    AccountHeadId = t1.AccountHeadId

                                }).ToList();

            List<VoucherDetail> voucherDetailList = new List<VoucherDetail>();
            if (!currentExistStock.Any())
            {
                foreach (var item in priviousStock.Where(x => x.DebitAmount > 0))
                {
                    VoucherDetail voucherDetail = new VoucherDetail
                    {
                        VoucherId = voucher.VoucherId,

                        AccountHeadId = item.AccountHeadId,
                        CreditAmount = item.DebitAmount,
                        DebitAmount = 0,
                        TransactionDate = DateTime.Now,
                        IsActive = true,
                        IsVirtual = true


                    };
                    voucherDetailList.Add(voucherDetail);
                    VoucherDetail voucherDetail1 = new VoucherDetail
                    {
                        VoucherId = voucher.VoucherId,

                        AccountHeadId = 50611914,
                        CreditAmount = 0,
                        DebitAmount = item.DebitAmount,
                        TransactionDate = DateTime.Now,
                        IsActive = true,
                        IsVirtual = true


                    };
                    voucherDetailList.Add(voucherDetail1);

                }
            }

            foreach (var item in currentStock)
            {


                VoucherDetail voucherDetail1 = new VoucherDetail
                {
                    VoucherId = voucher.VoucherId,

                    AccountHeadId = 50611914,
                    CreditAmount = item.DebitAmount,
                    DebitAmount = 0,
                    TransactionDate = DateTime.Now,
                    IsActive = true
                };
                voucherDetailList.Add(voucherDetail1);

            }

            _db.VoucherDetails.AddRange(voucherDetailList);

            if (await _db.SaveChangesAsync() > 0)
            {
                result = voucher.VoucherId;
            }

            return result;

        }

        public async Task<long> TradingAutoInsertStockVoucherDetails(int companyId, int voucherId)
        {
            long result = -1;

            var voucher = await _db.Vouchers.FindAsync(voucherId);

            var fromDate = voucher.VoucherDate.Value.AddDays(0 - voucher.VoucherDate.Value.Day).AddDays(-10);
            var todate = voucher.VoucherDate.Value.AddDays(0 - voucher.VoucherDate.Value.Day);
            var priviousStock = (from t1 in _db.VoucherDetails
                                 join t2 in _db.Vouchers on t1.VoucherId equals t2.VoucherId
                                 join headGL in _db.HeadGLs on t1.AccountHeadId equals headGL.Id
                                 join head5 in _db.Head5 on headGL.ParentId equals head5.Id
                                 join head4 in _db.Head4 on head5.ParentId equals head4.Id
                                 where t2.CompanyId == companyId && head5.AccCode == "1301005001"
                                 && t1.IsActive && t2.IsActive && !t1.IsVirtual &&
                                 t2.Accounting_CostCenterFk == voucher.Accounting_CostCenterFk &&
                                     t2.VoucherDate >= fromDate
                                     && t2.VoucherDate <= todate

                                 select new
                                 {
                                     AccountHeadId = t1.AccountHeadId,
                                     DebitAmount = t1.DebitAmount
                                 }).ToList();
            var fromExistDate = voucher.VoucherDate.Value.AddDays(-10);
            var currentExistStock = (from t1 in _db.VoucherDetails
                                     join t2 in _db.Vouchers on t1.VoucherId equals t2.VoucherId
                                     join headGL in _db.HeadGLs on t1.AccountHeadId equals headGL.Id
                                     join head5 in _db.Head5 on headGL.ParentId equals head5.Id
                                     join head4 in _db.Head4 on head5.ParentId equals head4.Id
                                     where t2.CompanyId == companyId && head5.AccCode == "1301005001"
                                     && t1.IsActive && t2.IsActive && !t1.IsVirtual &&

    t2.VoucherDate >= fromExistDate

                                                  && t2.VoucherDate <= voucher.VoucherDate.Value
                                                  && t1.VoucherId != voucher.VoucherId

                                     select new
                                     {
                                         AccountHeadId = t1.AccountHeadId,
                                         DebitAmount = t1.DebitAmount
                                     }).AsEnumerable();

            var currentStock = (from t1 in _db.VoucherDetails
                                where t1.VoucherId == voucher.VoucherId && t1.IsActive
                                select new
                                {
                                    DebitAmount = t1.DebitAmount,
                                    AccountHeadId = t1.AccountHeadId

                                }).ToList();

            List<VoucherDetail> voucherDetailList = new List<VoucherDetail>();
            if (!currentExistStock.Any())
            {
                foreach (var item in priviousStock.Where(x => x.DebitAmount > 0))
                {
                    VoucherDetail voucherDetail = new VoucherDetail
                    {
                        VoucherId = voucher.VoucherId,

                        AccountHeadId = item.AccountHeadId,
                        CreditAmount = item.DebitAmount,
                        DebitAmount = 0,
                        TransactionDate = DateTime.Now,
                        IsActive = true,
                        IsVirtual = true


                    };
                    voucherDetailList.Add(voucherDetail);
                    VoucherDetail voucherDetail1 = new VoucherDetail
                    {
                        VoucherId = voucher.VoucherId,

                        AccountHeadId = 50611914,
                        CreditAmount = 0,
                        DebitAmount = item.DebitAmount,
                        TransactionDate = DateTime.Now,
                        IsActive = true,
                        IsVirtual = true


                    };
                    voucherDetailList.Add(voucherDetail1);

                }
            }

            foreach (var item in currentStock)
            {


                VoucherDetail voucherDetail1 = new VoucherDetail
                {
                    VoucherId = voucher.VoucherId,

                    AccountHeadId = 50611914,
                    CreditAmount = item.DebitAmount,
                    DebitAmount = 0,
                    TransactionDate = DateTime.Now,
                    IsActive = true
                };
                voucherDetailList.Add(voucherDetail1);

            }

            _db.VoucherDetails.AddRange(voucherDetailList);

            if (await _db.SaveChangesAsync() > 0)
            {
                result = voucher.VoucherId;
            }

            return result;

        }

        public async Task<long> SafeFoodAutoInsertStockVoucherDetails(int companyId, int voucherId)
        {
            long result = -1;

            var voucher = await _db.Vouchers.FindAsync(voucherId);

            var fromDate = voucher.VoucherDate.Value.AddDays(0 - voucher.VoucherDate.Value.Day).AddDays(-10);
            var todate = voucher.VoucherDate.Value.AddDays(0 - voucher.VoucherDate.Value.Day);
            var priviousStock = (from t1 in _db.VoucherDetails
                                 join t2 in _db.Vouchers on t1.VoucherId equals t2.VoucherId
                                 join headGL in _db.HeadGLs on t1.AccountHeadId equals headGL.Id
                                 join head5 in _db.Head5 on headGL.ParentId equals head5.Id
                                 join head4 in _db.Head4 on head5.ParentId equals head4.Id
                                 where t2.CompanyId == companyId && head5.ParentId == 50611881
                                 && t1.IsActive && t2.IsActive && t2.IsStock &&
                                 t2.Accounting_CostCenterFk == voucher.Accounting_CostCenterFk &&
                                     t2.VoucherDate >= fromDate
                                     && t2.VoucherDate <= todate

                                 select new
                                 {
                                     AccountHeadId = t1.AccountHeadId,
                                     DebitAmount = t1.DebitAmount
                                 }).ToList();
            var fromExistDate = voucher.VoucherDate.Value.AddDays(-10);
            var currentExistStock = (from t1 in _db.VoucherDetails
                                     join t2 in _db.Vouchers on t1.VoucherId equals t2.VoucherId
                                     join headGL in _db.HeadGLs on t1.AccountHeadId equals headGL.Id
                                     join head5 in _db.Head5 on headGL.ParentId equals head5.Id
                                     join head4 in _db.Head4 on head5.ParentId equals head4.Id
                                     where t2.CompanyId == companyId && head5.ParentId == 50611881
                                     && t1.IsActive && t2.IsActive && t2.IsStock &&
                                     t2.Accounting_CostCenterFk == voucher.Accounting_CostCenterFk &&

    t2.VoucherDate >= fromExistDate

                                                  && t2.VoucherDate <= voucher.VoucherDate.Value
                                                  && t1.VoucherId != voucher.VoucherId

                                     select new
                                     {
                                         AccountHeadId = t1.AccountHeadId,
                                         DebitAmount = t1.DebitAmount
                                     }).AsEnumerable();

            var currentStock = (from t1 in _db.VoucherDetails
                                where t1.VoucherId == voucher.VoucherId && t1.IsActive
                                select new
                                {
                                    DebitAmount = t1.DebitAmount,
                                    AccountHeadId = t1.AccountHeadId

                                }).ToList();

            List<VoucherDetail> voucherDetailList = new List<VoucherDetail>();
            if (!currentExistStock.Any())
            {
                foreach (var item in priviousStock.Where(x => x.DebitAmount > 0))
                {
                    VoucherDetail voucherDetail = new VoucherDetail
                    {
                        VoucherId = voucher.VoucherId,

                        AccountHeadId = item.AccountHeadId,
                        CreditAmount = item.DebitAmount,
                        DebitAmount = 0,
                        TransactionDate = DateTime.Now,
                        IsActive = true,
                        IsVirtual = true


                    };
                    voucherDetailList.Add(voucherDetail);
                    VoucherDetail voucherDetail1 = new VoucherDetail
                    {
                        VoucherId = voucher.VoucherId,

                        AccountHeadId = 50612090,
                        CreditAmount = 0,
                        DebitAmount = item.DebitAmount,
                        TransactionDate = DateTime.Now,
                        IsActive = true,
                        IsVirtual = true


                    };
                    voucherDetailList.Add(voucherDetail1);

                }
            }

            foreach (var item in currentStock)
            {


                VoucherDetail voucherDetail1 = new VoucherDetail
                {
                    VoucherId = voucher.VoucherId,

                    AccountHeadId = 50612090,
                    CreditAmount = item.DebitAmount,
                    DebitAmount = 0,
                    TransactionDate = DateTime.Now,
                    IsActive = true
                };
                voucherDetailList.Add(voucherDetail1);

            }
            _db.VoucherDetails.AddRange(voucherDetailList);

            if (await _db.SaveChangesAsync() > 0)
            {
                result = voucher.VoucherId;
            }

            return result;

        }
        public async Task<List<HeadGLModel>> Head5Get(int companyId, int parentId)
        {

            List<HeadGLModel> head5List =
               await Task.Run(() => (from t1 in _db.Head5
                                     where t1.ParentId == parentId && t1.CompanyId == companyId
                                     select new HeadGLModel
                                     {
                                         Id = t1.Id,
                                         AccName = t1.AccCode + " -" + t1.AccName
                                     }).ToList());
            return head5List;
        }
        public async Task<List<HeadGLModel>> HeadGLGet(int companyId, int parentId)
        {

            List<HeadGLModel> headGLList =
               await Task.Run(() => (from t1 in _db.HeadGLs
                                     where t1.ParentId == parentId && t1.CompanyId == companyId
                                     select new HeadGLModel
                                     {
                                         Id = t1.Id,
                                         AccName = t1.AccCode + " -" + t1.AccName
                                     }).ToList());
            return headGLList;
        }
        public async Task<List<HeadGLModel>> HeadGLByHead5ParentIdGet(int companyId, int parentId)
        {

            List<HeadGLModel> HeadGLModelList =
               await Task.Run(() => (from t1 in _db.HeadGLs
                                     join t2 in _db.Head5 on t1.ParentId equals t2.Id
                                     where t2.ParentId == parentId && t1.CompanyId == companyId
                                     select new HeadGLModel
                                     {
                                         Id = t1.Id,
                                         AccName = t1.AccCode + " -" + t1.AccName
                                     }).ToList());
            return HeadGLModelList;
        }

        public async Task<List<HeadGLModel>> HeadGLByHeadGLParentIdGet(int companyId, int parentId)
        {

            List<HeadGLModel> HeadGLModelList =
               await Task.Run(() => (from t1 in _db.HeadGLs
                                     where t1.ParentId == parentId && t1.CompanyId == companyId
                                     select new HeadGLModel
                                     {
                                         Id = t1.Id,
                                         AccName = t1.AccCode + " -" + t1.AccName
                                     }).ToList());
            return HeadGLModelList;
        }

        public object GetAutoCompleteHeadGL(string prefix, int companyId)
        {
            var v = (from hgl in _db.HeadGLs
                     join h5 in _db.Head5 on hgl.ParentId equals h5.Id
                     join h4 in _db.Head4 on h5.ParentId equals h4.Id

                     where hgl.CompanyId == companyId && hgl.IsActive && h5.IsActive && h4.IsActive
                     && ((hgl.AccName.Contains(prefix)) || (hgl.AccCode.Contains(prefix)))
                     select new
                     {
                         label = "[" + hgl.AccCode + "] " + (h4.AccName == h5.AccName ? h5.AccName : h4.AccName + " " + h5.AccName) + " " + hgl.AccName,
                         val = hgl.Id
                     }).OrderBy(x => x.label).Take(200).ToList();

            return v;
        }
        public object GetAutoCompleteVendorHeadGL(string prefix, int companyId, int vendorTypeId)
        {
            //te
            var v = 0;


            return v;
        }

        public async Task<long> AutoInsertVoucherDetails(int voucherId, int virtualHeadId, string virtualHeadParticular)
        {
            long result = -1;

            var voucher = await _db.Vouchers.FindAsync(voucherId);
            double totalDebitAmount = _db.VoucherDetails.Where(x => x.VoucherId == voucherId && x.IsActive == true).Select(x => x.DebitAmount).DefaultIfEmpty(0).Sum();
            double totalCreditAmount = _db.VoucherDetails.Where(x => x.VoucherId == voucherId && x.IsActive == true).Select(x => x.CreditAmount).DefaultIfEmpty(0).Sum();
            double newAmount = 0;
            if (totalDebitAmount > totalCreditAmount)
            {
                newAmount = totalDebitAmount - totalCreditAmount;
            }
            else
            {
                newAmount = totalCreditAmount - totalDebitAmount;
            }
            if (newAmount > 0 && virtualHeadId > 0)
            {
                VoucherDetail voucherDetail = new VoucherDetail
                {
                    VoucherId = voucher.VoucherId,

                    AccountHeadId = virtualHeadId,
                    CreditAmount = totalDebitAmount > totalCreditAmount ? newAmount : 0,
                    DebitAmount = totalCreditAmount > totalDebitAmount ? newAmount : 0,
                    TransactionDate = DateTime.Now,
                    IsActive = true,
                    Particular = virtualHeadParticular
                };
                _db.VoucherDetails.Add(voucherDetail);
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = voucherDetail.VoucherDetailId;
                }
            }

            return result;

        }



        public async Task<long> NFFLAutoInsertStockVoucherDetails(int companyId, int voucherId)
        {
            long result = -1;

            var voucher = await _db.Vouchers.FindAsync(voucherId);

            var fromDate = voucher.VoucherDate.Value.AddDays(0 - voucher.VoucherDate.Value.Day).AddDays(-10);
            var todate = voucher.VoucherDate.Value.AddDays(0 - voucher.VoucherDate.Value.Day);
            var priviousStock = (from t1 in _db.VoucherDetails
                                 join t2 in _db.Vouchers on t1.VoucherId equals t2.VoucherId
                                 join t3 in _db.HeadGLs on t1.AccountHeadId equals t3.Id
                                 join t4 in _db.Head5 on t3.ParentId equals t4.Id
                                 where t2.CompanyId == companyId && t4.AccCode == "1301004001"
                                 && t1.IsActive && t2.IsActive && !t1.IsVirtual
                                 && t2.Accounting_CostCenterFk == voucher.Accounting_CostCenterFk &&
                                     t2.VoucherDate >= fromDate
                                     && t2.VoucherDate <= todate


                                 select new
                                 {
                                     AccountHeadId = t1.AccountHeadId,
                                     DebitAmount = t1.DebitAmount
                                 }).ToList();
            var fromExistDate = voucher.VoucherDate.Value.AddDays(-10);
            var currentstockExist = (from t1 in _db.VoucherDetails
                                     join t2 in _db.Vouchers on t1.VoucherId equals t2.VoucherId
                                     join t3 in _db.HeadGLs on t1.AccountHeadId equals t3.Id
                                     join t4 in _db.Head5 on t3.ParentId equals t4.Id
                                     where t2.CompanyId == companyId && t4.AccCode == "1301004001"
                                     && t1.IsActive && t2.IsActive && !t1.IsVirtual &&
                                         t2.VoucherDate >= fromExistDate
                                         && t2.VoucherDate <= voucher.VoucherDate.Value
                                                  && t1.VoucherId != voucher.VoucherId

                                     select new
                                     {
                                         AccountHeadId = t1.AccountHeadId,
                                         DebitAmount = t1.DebitAmount
                                     }).AsEnumerable();

            var currentStock = (from t1 in _db.VoucherDetails
                                where t1.VoucherId == voucher.VoucherId && t1.IsActive
                                select new
                                {
                                    DebitAmount = t1.DebitAmount,
                                    AccountHeadId = t1.AccountHeadId
                                }).ToList();

            List<VoucherDetail> voucherDetailList = new List<VoucherDetail>();
            if (!currentstockExist.Any())
            {
                foreach (var item in priviousStock.Where(x => x.DebitAmount > 0))
                {
                    VoucherDetail voucherDetail = new VoucherDetail
                    {
                        VoucherId = voucher.VoucherId,

                        AccountHeadId = item.AccountHeadId,
                        CreditAmount = item.DebitAmount,
                        DebitAmount = 0,
                        TransactionDate = DateTime.Now,
                        IsActive = true,
                        IsVirtual = true
                    };
                    voucherDetailList.Add(voucherDetail);
                    VoucherDetail voucherDetail1 = new VoucherDetail
                    {
                        VoucherId = voucher.VoucherId,

                        AccountHeadId = 50601365,
                        CreditAmount = 0,
                        DebitAmount = item.DebitAmount,
                        TransactionDate = DateTime.Now,
                        IsActive = true,
                        IsVirtual = true


                    };
                    voucherDetailList.Add(voucherDetail1);

                }
            }

            foreach (var item in currentStock)
            {


                VoucherDetail voucherDetail1 = new VoucherDetail
                {
                    VoucherId = voucher.VoucherId,

                    AccountHeadId = 50601365,
                    CreditAmount = item.DebitAmount,
                    DebitAmount = 0,
                    TransactionDate = DateTime.Now,
                    IsActive = true
                };
                voucherDetailList.Add(voucherDetail1);

            }

            _db.VoucherDetails.AddRange(voucherDetailList);

            if (await _db.SaveChangesAsync() > 0)
            {
                result = voucher.VoucherId;
            }

            return result;

        }

        public async Task<long> OPLAutoInsertStockVoucherDetails(int companyId, int voucherId)
        {
            long result = -1;

            var voucher = await _db.Vouchers.FindAsync(voucherId);

            var fromDate = voucher.VoucherDate.Value.AddDays(0 - voucher.VoucherDate.Value.Day).AddDays(-10);
            var todate = voucher.VoucherDate.Value.AddDays(0 - voucher.VoucherDate.Value.Day);
            var priviousStock = (from t1 in _db.VoucherDetails
                                 join t2 in _db.Vouchers on t1.VoucherId equals t2.VoucherId
                                 join t3 in _db.HeadGLs on t1.AccountHeadId equals t3.Id
                                 join t4 in _db.Head5 on t3.ParentId equals t4.Id
                                 join t5 in _db.Head4 on t4.ParentId equals t5.Id
                                 where t2.CompanyId == companyId && t5.AccCode == "1301004"
                                 && t1.IsActive && t2.IsActive && !t1.IsVirtual &&
                                 t2.Accounting_CostCenterFk == voucher.Accounting_CostCenterFk &&
                                     t2.VoucherDate >= fromDate
                                     && t2.VoucherDate <= todate

                                 select new
                                 {
                                     AccountHeadId = t1.AccountHeadId,
                                     DebitAmount = t1.DebitAmount
                                 }).ToList();

            var fromExistDate = voucher.VoucherDate.Value.AddDays(-10);

            var curentExistStock = (from t1 in _db.VoucherDetails
                                    join t2 in _db.Vouchers on t1.VoucherId equals t2.VoucherId
                                    join t3 in _db.HeadGLs on t1.AccountHeadId equals t3.Id
                                    join t4 in _db.Head5 on t3.ParentId equals t4.Id
                                    join t5 in _db.Head4 on t4.ParentId equals t5.Id
                                    where t2.CompanyId == companyId && t5.AccCode == "1301004"
                                    && t1.IsActive && t2.IsActive && !t1.IsVirtual &&
                                        t2.VoucherDate >= fromExistDate
                                        && t2.VoucherDate <= voucher.VoucherDate.Value
                                                 && t1.VoucherId != voucher.VoucherId

                                    select new
                                    {
                                        AccountHeadId = t1.AccountHeadId,
                                        DebitAmount = t1.DebitAmount
                                    }).AsEnumerable();
            var currentStock = (from t1 in _db.VoucherDetails
                                where t1.VoucherId == voucher.VoucherId && t1.IsActive
                                select new
                                {
                                    DebitAmount = t1.DebitAmount,
                                    AccountHeadId = t1.AccountHeadId

                                }).ToList();

            List<VoucherDetail> voucherDetailList = new List<VoucherDetail>();
            if (!curentExistStock.Any())
            {
                foreach (var item in priviousStock.Where(x => x.DebitAmount > 0))
                {
                    VoucherDetail voucherDetail = new VoucherDetail
                    {
                        VoucherId = voucher.VoucherId,

                        AccountHeadId = item.AccountHeadId,
                        CreditAmount = item.DebitAmount,
                        DebitAmount = 0,
                        TransactionDate = DateTime.Now,
                        IsActive = true,
                        IsVirtual = true


                    };
                    voucherDetailList.Add(voucherDetail);
                    VoucherDetail voucherDetail1 = new VoucherDetail
                    {
                        VoucherId = voucher.VoucherId,

                        AccountHeadId = 50607042,
                        CreditAmount = 0,
                        DebitAmount = item.DebitAmount,
                        TransactionDate = DateTime.Now,
                        IsActive = true,
                        IsVirtual = true


                    };
                    voucherDetailList.Add(voucherDetail1);

                }
            }

            foreach (var item in currentStock)
            {


                VoucherDetail voucherDetail1 = new VoucherDetail
                {
                    VoucherId = voucher.VoucherId,

                    AccountHeadId = 50607042,
                    CreditAmount = item.DebitAmount,
                    DebitAmount = 0,
                    TransactionDate = DateTime.Now,
                    IsActive = true
                };
                voucherDetailList.Add(voucherDetail1);

            }

            _db.VoucherDetails.AddRange(voucherDetailList);

            if (await _db.SaveChangesAsync() > 0)
            {
                result = voucher.VoucherId;
            }

            return result;

        }

        public async Task<long> SODLAutoInsertStockVoucherDetails(int companyId, int voucherId)
        {
            long result = -1;

            var voucher = await _db.Vouchers.FindAsync(voucherId);

            var fromDate = voucher.VoucherDate.Value.AddDays(0 - voucher.VoucherDate.Value.Day).AddDays(-10);
            var todate = voucher.VoucherDate.Value.AddDays(0 - voucher.VoucherDate.Value.Day);

            var priviousStock = (from t1 in _db.VoucherDetails
                                 join t2 in _db.Vouchers on t1.VoucherId equals t2.VoucherId
                                 join t3 in _db.HeadGLs on t1.AccountHeadId equals t3.Id
                                 join t4 in _db.Head5 on t3.ParentId equals t4.Id
                                 join t5 in _db.Head4 on t4.ParentId equals t5.Id
                                 where t2.CompanyId == companyId && t5.AccCode == "1301004"
                                 && t1.IsActive && t2.IsActive && !t1.IsVirtual &&
                                 t2.Accounting_CostCenterFk == voucher.Accounting_CostCenterFk &&
                                     t2.VoucherDate >= fromDate
                                     && t2.VoucherDate <= todate

                                 select new
                                 {
                                     AccountHeadId = t1.AccountHeadId,
                                     DebitAmount = t1.DebitAmount
                                 }).ToList();
            var fromExistDate = voucher.VoucherDate.Value.AddDays(-10);

            var existCorrentMonthStock = (from t1 in _db.VoucherDetails
                                          join t2 in _db.Vouchers on t1.VoucherId equals t2.VoucherId
                                          join t3 in _db.HeadGLs on t1.AccountHeadId equals t3.Id
                                          join t4 in _db.Head5 on t3.ParentId equals t4.Id
                                          join t5 in _db.Head4 on t4.ParentId equals t5.Id
                                          where t2.CompanyId == companyId && t5.AccCode == "1301004"
                                          && t1.IsActive && t2.IsActive && !t1.IsVirtual &&
                                              t2.VoucherDate >= fromExistDate
                                              && t2.VoucherDate <= voucher.VoucherDate.Value
                                              && t1.VoucherId != voucher.VoucherId
                                          select new
                                          {
                                              AccountHeadId = t1.AccountHeadId,
                                              DebitAmount = t1.DebitAmount
                                          }).AsEnumerable();

            var currentStock = (from t1 in _db.VoucherDetails
                                where t1.VoucherId == voucher.VoucherId && t1.IsActive
                                select new
                                {
                                    DebitAmount = t1.DebitAmount,
                                    AccountHeadId = t1.AccountHeadId

                                }).ToList();

            List<VoucherDetail> voucherDetailList = new List<VoucherDetail>();
            if (!existCorrentMonthStock.Any())
            {
                foreach (var item in priviousStock.Where(x => x.DebitAmount > 0))
                {
                    VoucherDetail voucherDetail = new VoucherDetail
                    {
                        VoucherId = voucher.VoucherId,

                        AccountHeadId = item.AccountHeadId,
                        CreditAmount = item.DebitAmount,
                        DebitAmount = 0,
                        TransactionDate = DateTime.Now,
                        IsActive = true,
                        IsVirtual = true


                    };
                    voucherDetailList.Add(voucherDetail);
                    VoucherDetail voucherDetail1 = new VoucherDetail
                    {
                        VoucherId = voucher.VoucherId,

                        AccountHeadId = 50607044,
                        CreditAmount = 0,
                        DebitAmount = item.DebitAmount,
                        TransactionDate = DateTime.Now,
                        IsActive = true,
                        IsVirtual = true


                    };
                    voucherDetailList.Add(voucherDetail1);

                }
            }

            foreach (var item in currentStock)
            {


                VoucherDetail voucherDetail1 = new VoucherDetail
                {
                    VoucherId = voucher.VoucherId,

                    AccountHeadId = 50607044,
                    CreditAmount = item.DebitAmount,
                    DebitAmount = 0,
                    TransactionDate = DateTime.Now,
                    IsActive = true
                };
                voucherDetailList.Add(voucherDetail1);

            }

            _db.VoucherDetails.AddRange(voucherDetailList);

            if (await _db.SaveChangesAsync() > 0)
            {
                result = voucher.VoucherId;
            }

            return result;

        }

        public async Task<long> PrintingAutoInsertStockVoucherDetails(int companyId, int voucherId)
        {
            long result = -1;

            var voucher = await _db.Vouchers.FindAsync(voucherId);

            var fromDate = voucher.VoucherDate.Value.AddDays(0 - voucher.VoucherDate.Value.Day).AddDays(-10);
            var todate = voucher.VoucherDate.Value.AddDays(0 - voucher.VoucherDate.Value.Day);
            var priviousStock = (from t1 in _db.VoucherDetails
                                 join t2 in _db.Vouchers on t1.VoucherId equals t2.VoucherId
                                 join headGL in _db.HeadGLs on t1.AccountHeadId equals headGL.Id
                                 join head5 in _db.Head5 on headGL.ParentId equals head5.Id
                                 join head4 in _db.Head4 on head5.ParentId equals head4.Id
                                 where t2.CompanyId == companyId && head4.AccCode == "1305001"
                                 && t1.IsActive && t2.IsActive && !t1.IsVirtual &&
                                 t2.Accounting_CostCenterFk == voucher.Accounting_CostCenterFk &&
                                     t2.VoucherDate >= fromDate
                                     && t2.VoucherDate <= todate

                                 select new
                                 {
                                     AccountHeadId = t1.AccountHeadId,
                                     DebitAmount = t1.DebitAmount
                                 }).ToList();
            var fromExistDate = voucher.VoucherDate.Value.AddDays(-10);

            var currentExistStock = (from t1 in _db.VoucherDetails
                                     join t2 in _db.Vouchers on t1.VoucherId equals t2.VoucherId
                                     join headGL in _db.HeadGLs on t1.AccountHeadId equals headGL.Id
                                     join head5 in _db.Head5 on headGL.ParentId equals head5.Id
                                     join head4 in _db.Head4 on head5.ParentId equals head4.Id
                                     where t2.CompanyId == companyId && head4.AccCode == "1305001"
                                     && t1.IsActive && t2.IsActive && !t1.IsVirtual &&
                                          t2.VoucherDate >= fromExistDate
                                                  && t2.VoucherDate <= voucher.VoucherDate.Value
                                                  && t1.VoucherId != voucher.VoucherId

                                     select new
                                     {
                                         AccountHeadId = t1.AccountHeadId,
                                         DebitAmount = t1.DebitAmount
                                     }).AsEnumerable();
            var currentStock = (from t1 in _db.VoucherDetails
                                where t1.VoucherId == voucher.VoucherId && t1.IsActive
                                select new
                                {
                                    DebitAmount = t1.DebitAmount,
                                    AccountHeadId = t1.AccountHeadId

                                }).ToList();

            List<VoucherDetail> voucherDetailList = new List<VoucherDetail>();
            if (!currentExistStock.Any())
            {
                foreach (var item in priviousStock.Where(x => x.DebitAmount > 0))
                {
                    VoucherDetail voucherDetail = new VoucherDetail
                    {
                        VoucherId = voucher.VoucherId,

                        AccountHeadId = item.AccountHeadId,
                        CreditAmount = item.DebitAmount,
                        DebitAmount = 0,
                        TransactionDate = DateTime.Now,
                        IsActive = true,
                        IsVirtual = true


                    };
                    voucherDetailList.Add(voucherDetail);
                    VoucherDetail voucherDetail1 = new VoucherDetail
                    {
                        VoucherId = voucher.VoucherId,

                        AccountHeadId = 50607050,
                        CreditAmount = 0,
                        DebitAmount = item.DebitAmount,
                        TransactionDate = DateTime.Now,
                        IsActive = true,
                        IsVirtual = true


                    };
                    voucherDetailList.Add(voucherDetail1);

                }
            }

            foreach (var item in currentStock)
            {


                VoucherDetail voucherDetail1 = new VoucherDetail
                {
                    VoucherId = voucher.VoucherId,

                    AccountHeadId = 50607050,
                    CreditAmount = item.DebitAmount,
                    DebitAmount = 0,
                    TransactionDate = DateTime.Now,
                    IsActive = true
                };
                voucherDetailList.Add(voucherDetail1);
            }

            _db.VoucherDetails.AddRange(voucherDetailList);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = voucher.VoucherId;
            }
            return result;

        }

        public async Task<long> FBLAutoInsertStockVoucherDetails(int companyId, int voucherId)
        {
            long result = -1;

            var voucher = await _db.Vouchers.FindAsync(voucherId);

            var fromDate = voucher.VoucherDate.Value.AddDays(0 - voucher.VoucherDate.Value.Day).AddDays(-10);
            var todate = voucher.VoucherDate.Value.AddDays(0 - voucher.VoucherDate.Value.Day);
            var priviousStock = (from t1 in _db.VoucherDetails
                                 join t2 in _db.Vouchers on t1.VoucherId equals t2.VoucherId
                                 join headGL in _db.HeadGLs on t1.AccountHeadId equals headGL.Id
                                 join head5 in _db.Head5 on headGL.ParentId equals head5.Id
                                 join head4 in _db.Head4 on head5.ParentId equals head4.Id
                                 join head3 in _db.Head3 on head4.ParentId equals head3.Id

                                 where t2.CompanyId == companyId && head3.AccCode == "1305"
                                 && t1.IsActive && t2.IsActive && !t1.IsVirtual &&
                                 t2.Accounting_CostCenterFk == voucher.Accounting_CostCenterFk &&
                                     t2.VoucherDate >= fromDate
                                     && t2.VoucherDate <= todate

                                 select new
                                 {
                                     AccountHeadId = t1.AccountHeadId,
                                     DebitAmount = t1.DebitAmount
                                 }).ToList();

            var startDateOfCurrentMonth = new DateTime(voucher.VoucherDate.Value.Year, voucher.VoucherDate.Value.Month, 1);
            var endDateOfCurrentMonth = startDateOfCurrentMonth.AddMonths(1).AddDays(-1);

            //var fromExistDate = voucher.VoucherDate.Value.AddDays(-10);

            var currentExistStock = (from t1 in _db.VoucherDetails
                                     join t2 in _db.Vouchers on t1.VoucherId equals t2.VoucherId
                                     join headGL in _db.HeadGLs on t1.AccountHeadId equals headGL.Id
                                     join head5 in _db.Head5 on headGL.ParentId equals head5.Id
                                     join head4 in _db.Head4 on head5.ParentId equals head4.Id
                                     join head3 in _db.Head3 on head4.ParentId equals head3.Id

                                     where t2.CompanyId == companyId && head3.AccCode == "1305"
                                     && t1.IsActive && t2.IsActive && !t1.IsVirtual && t2.IsStock &&
                                         t2.VoucherDate >= startDateOfCurrentMonth && t2.VoucherDate <= endDateOfCurrentMonth
                                         && t1.VoucherId != voucher.VoucherId


                                     select new
                                     {
                                         AccountHeadId = t1.AccountHeadId,
                                         DebitAmount = t1.DebitAmount
                                     }).AsEnumerable();

            var currentStock = (from t1 in _db.VoucherDetails
                                where t1.VoucherId == voucher.VoucherId && t1.IsActive
                                select new
                                {
                                    DebitAmount = t1.DebitAmount,
                                    AccountHeadId = t1.AccountHeadId
                                }).ToList();

            List<VoucherDetail> voucherDetailList = new List<VoucherDetail>();
            if (!currentExistStock.Any())
            {
                foreach (var item in priviousStock.Where(x => x.DebitAmount > 0))
                {
                    VoucherDetail voucherDetail = new VoucherDetail
                    {
                        VoucherId = voucher.VoucherId,
                        AccountHeadId = item.AccountHeadId,
                        CreditAmount = item.DebitAmount,
                        DebitAmount = 0,
                        TransactionDate = DateTime.Now,
                        IsActive = true,
                        IsVirtual = true
                    };
                    voucherDetailList.Add(voucherDetail);
                    VoucherDetail voucherDetail1 = new VoucherDetail
                    {
                        VoucherId = voucher.VoucherId,
                        AccountHeadId = 50607519,
                        CreditAmount = 0,
                        DebitAmount = item.DebitAmount,
                        TransactionDate = DateTime.Now,
                        IsActive = true,
                        IsVirtual = true
                    };
                    voucherDetailList.Add(voucherDetail1);
                }
            }

            foreach (var item in currentStock)
            {

                VoucherDetail voucherDetail1 = new VoucherDetail
                {
                    VoucherId = voucher.VoucherId,

                    AccountHeadId = 50607519,
                    CreditAmount = item.DebitAmount,
                    DebitAmount = 0,
                    TransactionDate = DateTime.Now,
                    IsActive = true
                };
                voucherDetailList.Add(voucherDetail1);

            }

            _db.VoucherDetails.AddRange(voucherDetailList);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = voucher.VoucherId;
            }
            return result;
        }

        public async Task<long> PackagingAutoInsertStockVoucherDetails(int companyId, int voucherId)
        {
            long result = -1;

            var voucher = await _db.Vouchers.FindAsync(voucherId);

            var fromDate = voucher.VoucherDate.Value.AddDays(0 - voucher.VoucherDate.Value.Day).AddDays(-10);
            var todate = voucher.VoucherDate.Value.AddDays(0 - voucher.VoucherDate.Value.Day);
            var priviousStock = (from t1 in _db.VoucherDetails
                                 join t2 in _db.Vouchers on t1.VoucherId equals t2.VoucherId
                                 join headGL in _db.HeadGLs on t1.AccountHeadId equals headGL.Id
                                 join head5 in _db.Head5 on headGL.ParentId equals head5.Id
                                 join head4 in _db.Head4 on head5.ParentId equals head4.Id

                                 where t2.CompanyId == companyId && head4.AccCode == "1301005"
                                 && t1.IsActive && t2.IsActive && !t1.IsVirtual &&
                                 t2.Accounting_CostCenterFk == voucher.Accounting_CostCenterFk &&
                                     t2.VoucherDate >= fromDate
                                     && t2.VoucherDate <= todate

                                 select new
                                 {
                                     AccountHeadId = t1.AccountHeadId,
                                     DebitAmount = t1.DebitAmount
                                 }).ToList();

            var fromExistDate = voucher.VoucherDate.Value.AddDays(-10);

            var currentExistStock = (from t1 in _db.VoucherDetails
                                     join t2 in _db.Vouchers on t1.VoucherId equals t2.VoucherId
                                     join headGL in _db.HeadGLs on t1.AccountHeadId equals headGL.Id
                                     join head5 in _db.Head5 on headGL.ParentId equals head5.Id
                                     join head4 in _db.Head4 on head5.ParentId equals head4.Id

                                     where t2.CompanyId == companyId && head4.AccCode == "1301005"
                                     && t1.IsActive && t2.IsActive && !t1.IsVirtual &&
                                         t2.VoucherDate >= fromExistDate
                                                  && t2.VoucherDate <= voucher.VoucherDate.Value
                                                  && t1.VoucherId != voucher.VoucherId

                                     select new
                                     {
                                         AccountHeadId = t1.AccountHeadId,
                                         DebitAmount = t1.DebitAmount
                                     }).AsEnumerable();
            var currentStock = (from t1 in _db.VoucherDetails
                                where t1.VoucherId == voucher.VoucherId && t1.IsActive
                                select new
                                {
                                    DebitAmount = t1.DebitAmount,
                                    AccountHeadId = t1.AccountHeadId

                                }).ToList();

            List<VoucherDetail> voucherDetailList = new List<VoucherDetail>();
            if (!currentExistStock.Any())
            {
                foreach (var item in priviousStock.Where(x => x.DebitAmount > 0))
                {
                    VoucherDetail voucherDetail = new VoucherDetail
                    {
                        VoucherId = voucher.VoucherId,

                        AccountHeadId = item.AccountHeadId,
                        CreditAmount = item.DebitAmount,
                        DebitAmount = 0,
                        TransactionDate = DateTime.Now,
                        IsActive = true,
                        IsVirtual = true


                    };
                    voucherDetailList.Add(voucherDetail);
                    VoucherDetail voucherDetail1 = new VoucherDetail
                    {
                        VoucherId = voucher.VoucherId,

                        AccountHeadId = 50605003,
                        CreditAmount = 0,
                        DebitAmount = item.DebitAmount,
                        TransactionDate = DateTime.Now,
                        IsActive = true,
                        IsVirtual = true


                    };
                    voucherDetailList.Add(voucherDetail1);

                }
            }

            foreach (var item in currentStock)
            {
                VoucherDetail voucherDetail1 = new VoucherDetail
                {
                    VoucherId = voucher.VoucherId,

                    AccountHeadId = 50605003,
                    CreditAmount = item.DebitAmount,
                    DebitAmount = 0,
                    TransactionDate = DateTime.Now,
                    IsActive = true
                };
                voucherDetailList.Add(voucherDetail1);

            }

            _db.VoucherDetails.AddRange(voucherDetailList);

            if (await _db.SaveChangesAsync() > 0)
            {
                result = voucher.VoucherId;
            }

            return result;

        }

        public async Task<long> KBLAutoInsertStockVoucherDetails(int companyId, int voucherId)
        {
            long result = -1;

            var voucher = await _db.Vouchers.FindAsync(voucherId);

            var fromDate = voucher.VoucherDate.Value.AddDays(0 - voucher.VoucherDate.Value.Day).AddDays(-10);
            var todate = voucher.VoucherDate.Value.AddDays(0 - voucher.VoucherDate.Value.Day);
            var priviousStock = (from t1 in _db.VoucherDetails
                                 join t2 in _db.Vouchers on t1.VoucherId equals t2.VoucherId
                                 join headGL in _db.HeadGLs on t1.AccountHeadId equals headGL.Id
                                 join head5 in _db.Head5 on headGL.ParentId equals head5.Id
                                 join head4 in _db.Head4 on head5.ParentId equals head4.Id
                                 where t2.CompanyId == companyId && head5.AccCode == "1301005001"
                                 && t1.IsActive && t2.IsActive && !t1.IsVirtual &&
                                 t2.Accounting_CostCenterFk == voucher.Accounting_CostCenterFk &&
                                     t2.VoucherDate >= fromDate
                                     && t2.VoucherDate <= todate

                                 select new
                                 {
                                     AccountHeadId = t1.AccountHeadId,
                                     DebitAmount = t1.DebitAmount
                                 }).ToList();
            var fromExistDate = voucher.VoucherDate.Value.AddDays(-10);
            var currentExistStock = (from t1 in _db.VoucherDetails
                                     join t2 in _db.Vouchers on t1.VoucherId equals t2.VoucherId
                                     join headGL in _db.HeadGLs on t1.AccountHeadId equals headGL.Id
                                     join head5 in _db.Head5 on headGL.ParentId equals head5.Id
                                     join head4 in _db.Head4 on head5.ParentId equals head4.Id
                                     where t2.CompanyId == companyId && head5.AccCode == "1301005001"
                                     && t1.IsActive && t2.IsActive && !t1.IsVirtual &&

    t2.VoucherDate >= fromExistDate

                                                  && t2.VoucherDate <= voucher.VoucherDate.Value
                                                  && t1.VoucherId != voucher.VoucherId

                                     select new
                                     {
                                         AccountHeadId = t1.AccountHeadId,
                                         DebitAmount = t1.DebitAmount
                                     }).AsEnumerable();

            var currentStock = (from t1 in _db.VoucherDetails
                                where t1.VoucherId == voucher.VoucherId && t1.IsActive
                                select new
                                {
                                    DebitAmount = t1.DebitAmount,
                                    AccountHeadId = t1.AccountHeadId

                                }).ToList();

            List<VoucherDetail> voucherDetailList = new List<VoucherDetail>();
            if (!currentExistStock.Any())
            {
                foreach (var item in priviousStock.Where(x => x.DebitAmount > 0))
                {
                    VoucherDetail voucherDetail = new VoucherDetail
                    {
                        VoucherId = voucher.VoucherId,

                        AccountHeadId = item.AccountHeadId,
                        CreditAmount = item.DebitAmount,
                        DebitAmount = 0,
                        TransactionDate = DateTime.Now,
                        IsActive = true,
                        IsVirtual = true


                    };
                    voucherDetailList.Add(voucherDetail);
                    VoucherDetail voucherDetail1 = new VoucherDetail
                    {
                        VoucherId = voucher.VoucherId,

                        AccountHeadId = 50608410,
                        CreditAmount = 0,
                        DebitAmount = item.DebitAmount,
                        TransactionDate = DateTime.Now,
                        IsActive = true,
                        IsVirtual = true


                    };
                    voucherDetailList.Add(voucherDetail1);

                }
            }

            foreach (var item in currentStock)
            {


                VoucherDetail voucherDetail1 = new VoucherDetail
                {
                    VoucherId = voucher.VoucherId,

                    AccountHeadId = 50608410,
                    CreditAmount = item.DebitAmount,
                    DebitAmount = 0,
                    TransactionDate = DateTime.Now,
                    IsActive = true
                };
                voucherDetailList.Add(voucherDetail1);

            }

            _db.VoucherDetails.AddRange(voucherDetailList);

            if (await _db.SaveChangesAsync() > 0)
            {
                result = voucher.VoucherId;
            }

            return result;

        }

        public async Task<long> UpdateVoucherStatus(int voucherId)
        {
            long result = -1;
            var totalAmount = _db.VoucherDetails
                .Where(x => x.VoucherId == voucherId && x.IsActive == true && x.IsVirtual == false)
                .Select(x => x.CreditAmount)
                .DefaultIfEmpty(0)
                .Sum();

            Voucher voucher = await _db.Vouchers.FindAsync(voucherId);
            voucher.VoucherStatus = "A";
            voucher.IsSubmit = true;
            voucher.TotalAmount = totalAmount;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = voucher.VoucherId;

            }
            int erpSM = await SMSPush(voucher);
            return result;

        }

        private async Task<int> SMSPush(Voucher voucher)
        {
           
            return 1;
        }

        public async Task<VMJournalSlave> GetSingleVoucherDetails(int voucherDetailId)
        {
            var v = await Task.Run(() => (from t1 in _db.VoucherDetails
                                          join t2 in _db.HeadGLs on t1.AccountHeadId equals t2.Id


                                          where t1.VoucherDetailId == voucherDetailId
                                          select new VMJournalSlave
                                          {
                                              VoucherId = t1.VoucherId.Value,
                                              VoucherDetailId = t1.VoucherDetailId,
                                              Accounting_HeadFK = t1.AccountHeadId.Value,
                                              AccountingHeadName = "[" + t2.AccCode + "] " + t2.AccName,
                                              Debit = t1.DebitAmount,
                                              Credit = t1.CreditAmount,
                                              Particular = t1.Particular
                                          }).FirstOrDefault());
            return v;
        }
        public async Task<VMJournalSlave> GetSingleVoucher(int voucherId)
        {
            var v = await Task.Run(() => (from t1 in _db.Vouchers.Where(f => f.VoucherId == voucherId)
                                          select new VMJournalSlave
                                          {
                                              VoucherId = t1.VoucherId,
                                              VoucherNo = t1.VoucherNo,
                                              VoucherTypeId = t1.VoucherTypeId.Value,
                                              ChqDate = t1.ChqDate,
                                              ChqName = t1.ChqName,
                                              Narration = t1.Narration
                                          }).FirstOrDefault());
            return v;
        }

        public async Task<VoucherTypeModel> GetSingleVoucherTypes(int voucherTypesId)
        {
            var v = await Task.Run(() => (from t1 in _db.VoucherTypes
                                          where t1.VoucherTypeId == voucherTypesId
                                          select new VoucherTypeModel
                                          {
                                              Code = t1.Code,
                                              IsBankOrCash = t1.IsBankOrCash,
                                              VoucherTypeId = t1.VoucherTypeId,
                                              Name = t1.Name,
                                              IsActive = t1.IsActive
                                          }).FirstOrDefault());
            return v;
        }

        private bool VoucherMapping(long voucherId, int companyId, long integratedId, string integratedFrom)
        {
            var objectToSave = _db.VoucherMaps
               .SingleOrDefault(q => q.VoucherId == voucherId
               && q.IntegratedId == integratedId
               && q.CompanyId == companyId
               && q.IntegratedFrom == integratedFrom);


            if (objectToSave != null)
            {
                return false;
            }
            else
            {
                VoucherMap voucherMap = new VoucherMap();
                voucherMap.VoucherId = voucherId;
                voucherMap.IntegratedId = integratedId;
                voucherMap.CompanyId = companyId;
                voucherMap.IntegratedFrom = integratedFrom;

                _db.VoucherMaps.Add(voucherMap);

                return _db.SaveChanges() > 0;

            }

        }
       
        private object VoucherMapping(long voucherId, int companyId, long moneyReceiptId, object integratedFrom)
        {
            throw new NotImplementedException();
        }
        


    }
}

﻿using KGERP.Data.Models;
using KGERP.Service.Configuration;
using KGERP.Service.Implementation.Accounting;
using KGERP.Service.ServiceModel;
using KGERP.Utility;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace KGERP.Service.Implementation

{
    public class ConfigurationService
    {
        private readonly ERPEntities _db;

        public ConfigurationService(ERPEntities db)
        {
            _db = db;
        }
        //#region User role Menuitem
        public async Task<VMUserMenuAssignment> UserMenuAssignmentGet(VMUserMenuAssignment vmUserMenuAssignment)
        {
            VMUserMenuAssignment vmMenuAssignment = new VMUserMenuAssignment();
            vmMenuAssignment.CompanyFK = vmUserMenuAssignment.CompanyFK;
            var companySubMenus = await _db.CompanySubMenus.Where(x => x.CompanyId == vmUserMenuAssignment.CompanyFK).ToListAsync();
            var companySubMenusId = companySubMenus.Select(x => x.CompanySubMenuId).ToList();

            var companyUserMenus = await _db.CompanyUserMenus.Where(x => x.CompanyId == vmUserMenuAssignment.CompanyFK && x.UserId == vmUserMenuAssignment.UserId).ToListAsync();
            var companyUserMenus_SubMenuId = companyUserMenus.Select(x => x.CompanySubMenuId).ToList();

            var companySubMenusNotExistsOnUserMenus = companySubMenusId.Where(CompanySubMenuId => !companyUserMenus_SubMenuId.Contains(CompanySubMenuId)).ToList();

            var filteredCompanySubMenus = companySubMenus.Where(x => companySubMenusNotExistsOnUserMenus.Contains(x.CompanySubMenuId)).ToList();
            if (filteredCompanySubMenus.Count() > 0)
            {
                List<CompanyUserMenu> userMenuList = new List<CompanyUserMenu>();
                foreach (var subMenus in filteredCompanySubMenus)
                {
                    CompanyUserMenu userMenu = new CompanyUserMenu
                    {
                        CompanyMenuId = subMenus.CompanyMenuId.Value,
                        CompanySubMenuId = subMenus.CompanySubMenuId,
                        IsActive = false,
                        IsView = true,
                        CompanyId = vmUserMenuAssignment.CompanyFK,
                        UserId = vmUserMenuAssignment.UserId,
                        CreatedBy = System.Web.HttpContext.Current.User.Identity.Name,
                        CreatedDate = DateTime.Now
                    };

                    userMenuList.Add(userMenu);
                }

                _db.CompanyUserMenus.AddRange(userMenuList);
                try
                {
                    await _db.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    var x = ex.Message;
                }


            }
            vmMenuAssignment.DataList = await Task.Run(() => CompanyUserMenuDataLoad(vmUserMenuAssignment));
            vmMenuAssignment.CompanyFK = vmUserMenuAssignment.CompanyFK;
            vmMenuAssignment.UserId = vmUserMenuAssignment.UserId;
            vmMenuAssignment.CompanyList = new SelectList(CompaniesDropDownList(), "Value", "Text");

            return vmMenuAssignment;
        }
        //public async Task<VMUserMenuAssignment> UserMenuAssignmentGet(VMUserMenuAssignment vmUserMenuAssignment)
        //{
        //    VMUserMenuAssignment vmMenuAssignment = new VMUserMenuAssignment();
        //    vmMenuAssignment.CompanyFK = vmUserMenuAssignment.CompanyFK;
        //    var companySubMenus = await _db.CompanySubMenus.Where(x => x.CompanyId == vmUserMenuAssignment.CompanyFK).ToListAsync();
        //    var companySubMenusId = companySubMenus.Select(x => x.CompanySubMenuId).ToList();

        //    var companyUserMenus =await _db.CompanyUserMenus.Where(x => x.CompanyId == vmUserMenuAssignment.CompanyFK && x.UserId == vmUserMenuAssignment.UserId).ToListAsync();
        //    var companyUserMenus_SubMenuId = companyUserMenus.Select(x => x.CompanySubMenuId).ToList();

        //    var companySubMenusNotExistsOnUserMenus = companySubMenusId.Where(CompanySubMenuId => !companyUserMenus_SubMenuId.Contains(CompanySubMenuId)).ToList();

        //    var filteredCompanySubMenus = companySubMenus.Where(x => companySubMenusNotExistsOnUserMenus.Contains(x.CompanySubMenuId)).ToList();
        //    if (filteredCompanySubMenus.Count() > 0)
        //    {
        //        List<CompanyUserMenu> userMenuList = new List<CompanyUserMenu>();
        //        foreach (var subMenus in filteredCompanySubMenus)
        //        {
        //            CompanyUserMenu userMenu = new CompanyUserMenu
        //            {
        //                CompanyMenuId = subMenus.CompanyMenuId.Value,
        //                CompanySubMenuId = subMenus.CompanySubMenuId,
        //                IsActive = false,
        //                IsView = true,
        //                CompanyId = vmUserMenuAssignment.CompanyFK,
        //                UserId = vmUserMenuAssignment.UserId,
        //                CreatedBy = System.Web.HttpContext.Current.User.Identity.Name,
        //                CreatedDate = DateTime.Now
        //            };

        //            userMenuList.Add(userMenu);
        //        }

        //        _db.CompanyUserMenus.AddRange(userMenuList);
        //        try
        //        {
        //            await _db.SaveChangesAsync();
        //        }
        //        catch(Exception ex)
        //        {
        //            var x = ex.Message;
        //        }


        //    }
        //    vmMenuAssignment.DataList = await Task.Run(() => CompanyUserMenuDataLoad(vmUserMenuAssignment));
        //    vmMenuAssignment.CompanyFK = vmUserMenuAssignment.CompanyFK;
        //    vmMenuAssignment.UserId = vmUserMenuAssignment.UserId;
        //    vmMenuAssignment.CompanyList = new SelectList(CompaniesDropDownList(), "Value", "Text");

        //    return vmMenuAssignment;
        //}



        public IEnumerable<VMUserMenuAssignment> CompanyUserMenuDataLoad(VMUserMenuAssignment vmMenuAssignment)
        {
            var v = (from t1 in _db.CompanyUserMenus
                     join t2 in _db.CompanySubMenus on t1.CompanySubMenuId equals t2.CompanySubMenuId
                     join t3 in _db.CompanyMenus on t1.CompanyMenuId equals t3.CompanyMenuId
                     join t4 in _db.Companies on t2.CompanyId equals t4.CompanyId
                     where t1.UserId == vmMenuAssignment.UserId && t1.CompanyId == vmMenuAssignment.CompanyFK
                     select new VMUserMenuAssignment
                     {
                         CompanyName = t4.Name,
                         MenuName = t3.Name,
                         SubmenuName = t2.Name,
                         Method = t2.Action + "/" + t2.Controller,

                         SubmenuID = t2.CompanySubMenuId,
                         IsActive = t1.IsActive,
                         MenuPriority = t2.OrderNo,


                         MenuID = t3.CompanyMenuId,
                         CompanyUserMenusId = t1.CompanyUserMenuId,
                         UserId = t1.UserId,
                         CompanyFK = t1.CompanyId,
                     }).OrderBy(x => x.MenuPriority).AsEnumerable();
            return v;
        }


        //public async Task<int> UserRoleMenuItemAdd(VMUserRoleMenuItem vmUserRoleMenuItem)
        //{
        //    var result = -1;
        //    User_RoleMenuItem userRoleMenuItem = new User_RoleMenuItem
        //    {
        //        IsAllowed = vmUserRoleMenuItem.IsAllowed,
        //        User_MenuItemFk = vmUserRoleMenuItem.ID,
        //        User_RoleFK = vmUserRoleMenuItem.ID,
        //        User = vmUserRoleMenuItem.User,
        //        UserID = vmUserRoleMenuItem.UserID
        //    };
        //    _db.User_RoleMenuItem.Add(userRoleMenuItem);
        //    if (await _db.SaveChangesAsync() > 0)
        //    {
        //        result = userRoleMenuItem.ID;
        //    }
        //    return result;
        //}
        public CompanyUserMenu CompanyUserMenuEdit(VMUserMenuAssignment vmUserMenuAssignment)
        {
            long result = -1;
            //to select Accountining_Chart_Two data.....
            CompanyUserMenu companyUserMenus = _db.CompanyUserMenus.Find(vmUserMenuAssignment.CompanyUserMenusId);
            companyUserMenus.IsActive = vmUserMenuAssignment.IsActive;
            companyUserMenus.ModifiedBy = System.Web.HttpContext.Current.User.Identity.Name;
            companyUserMenus.ModifiedDate = DateTime.Now;

            if (_db.SaveChanges() > 0)
            {
                result = companyUserMenus.CompanyUserMenuId;
            }
            return companyUserMenus;
        }
        ////public async (Task<int>, Task<bool>) UserRoleMenuItemEdit(VMUserRoleMenuItem vmUserRoleMenuItem)
        ////{
        ////    var result = -1;
        ////    //to select Accountining_Chart_Two data.....
        ////    User_RoleMenuItem userRoleMenuItem = _db.User_RoleMenuItem.Find(vmUserRoleMenuItem.ID);
        ////    userRoleMenuItem.IsAllowed = vmUserRoleMenuItem.IsAllowed;
        ////    userRoleMenuItem.User = vmUserRoleMenuItem.User;

        ////    if (await _db.SaveChangesAsync() > 0)
        ////    {
        ////        result = userRoleMenuItem.ID;
        ////    }
        ////    return result, false;
        ////}
        //public async Task<int> UserRoleMenuItemDelete(int id)
        //{
        //    int result = -1;

        //    if (id != 0)
        //    {
        //        User_RoleMenuItem userRoleMenuItem = _db.User_RoleMenuItem.Find(id);
        //        userRoleMenuItem.Active = false;

        //        if (await _db.SaveChangesAsync() > 0)
        //        {
        //            result = userRoleMenuItem.ID;
        //        }
        //    }
        //    return result;
        //}
        //#endregion
        public async Task<VMUserMenu> AccountingCostCenterGet(int companyId)
        {
            VMUserMenu vmUserMenu = new VMUserMenu();
            vmUserMenu.CompanyFK = companyId;
            vmUserMenu.DataList = (from t1 in _db.Accounting_CostCenter
                                   join t2 in _db.Companies on t1.CompanyId equals t2.CompanyId
                                   where t1.CompanyId == companyId && t1.IsActive
                                   select new VMUserMenu
                                   {
                                       ID = t1.CostCenterId,
                                       Name = t1.Name,
                                       CompanyName = t2.Name,
                                       CompanyFK = t1.CompanyId
                                   }).OrderByDescending(x => x.ID).AsEnumerable();
            return vmUserMenu;
        }

        public async Task<int> AccountingCostCenterAdd(VMUserMenu vmUserMenu)
        {
            var result = -1;


            Accounting_CostCenter costCenter = new Accounting_CostCenter
            {

                Name = vmUserMenu.Name,

                CompanyId = vmUserMenu.CompanyFK.Value,
                CreatedBy = System.Web.HttpContext.Current.User.Identity.Name,
                CreatedDate = DateTime.Now,
                IsActive = true
            };
            _db.Accounting_CostCenter.Add(costCenter);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = costCenter.CostCenterId;
            }
            return result;
        }
        public async Task<int> AccountingCostCenterEdit(VMUserMenu vmUserMenu)
        {
            var result = -1;
            Accounting_CostCenter costCenter = _db.Accounting_CostCenter.Find(vmUserMenu.ID);
            costCenter.Name = vmUserMenu.Name;

            costCenter.ModifiedBy = System.Web.HttpContext.Current.User.Identity.Name;


            if (await _db.SaveChangesAsync() > 0)
            {
                result = costCenter.CostCenterId;
            }
            return result;
        }
        public async Task<int> AccountingCostCenterDelete(int id)
        {
            int result = -1;
            if (id != 0)
            {
                Accounting_CostCenter costCenter = _db.Accounting_CostCenter.Find(id);
                costCenter.IsActive = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = costCenter.CostCenterId;
                }
            }
            return result;
        }

        #region User Menu
        public async Task<VMUserMenu> UserMenuGet()
        {
            VMUserMenu vmUserMenu = new VMUserMenu();
            vmUserMenu.DataList = await Task.Run(() => UserMenuDataLoad());
            return vmUserMenu;
        }

        public IEnumerable<VMUserMenu> UserMenuDataLoad()
        {
            var v = (from t1 in _db.CompanyMenus
                     join t2 in _db.Companies on t1.CompanyId equals t2.CompanyId
                     where t1.IsActive == true
                     select new VMUserMenu
                     {
                         ID = t1.CompanyMenuId,
                         Name = t1.Name,
                         CompanyName = t2.Name,
                         LayerNo = t1.LayerNo,
                         ShortName = t1.ShortName,
                         Priority = t1.OrderNo,
                         IsActive = t1.IsActive,
                         CompanyFK = t1.CompanyId
                     }).OrderByDescending(x => x.ID).AsEnumerable();
            return v;
        }


        public async Task<int> UserMenuAdd(VMUserMenu vmUserMenu)
        {
            var result = -1;


            CompanyMenu userMenu = new CompanyMenu
            {
                CompanyMenuId = _db.Database.SqlQuery<int>("exec spGetNewCompanyId").FirstOrDefault(),
                Name = vmUserMenu.Name,
                OrderNo = vmUserMenu.Priority,
                LayerNo = vmUserMenu.LayerNo,
                ShortName = vmUserMenu.Name,

                CompanyId = vmUserMenu.CompanyFK,
                CreatedBy = System.Web.HttpContext.Current.User.Identity.Name,
                CreatedDate = DateTime.Now,
                IsActive = true
            };
            _db.CompanyMenus.Add(userMenu);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = userMenu.CompanyMenuId;
            }
            return result;
        }
        public async Task<int> UserMenuEdit(VMUserMenu vmUserMenu)
        {
            var result = -1;
            using (DbContextTransaction dbTran = _db.Database.BeginTransaction())
            {
                try
                {
                    CompanyMenu userMenu = _db.CompanyMenus.Find(vmUserMenu.ID);
                    var CompanyUserMenuList = await _db.CompanyUserMenus
                       .Where(e => e.CompanyMenuId == vmUserMenu.ID
                       && e.CompanyId == userMenu.CompanyId && e.IsActive == true).ToListAsync();
                    CompanyUserMenuList.ForEach(e => e.CompanyId = vmUserMenu.CompanyFK);

                    userMenu.Name = vmUserMenu.Name;
                    userMenu.CompanyId = vmUserMenu.CompanyFK;

                    userMenu.OrderNo = vmUserMenu.Priority;
                    userMenu.LayerNo = vmUserMenu.LayerNo;
                    userMenu.ShortName = vmUserMenu.ShortName;
                    userMenu.ModifiedBy = System.Web.HttpContext.Current.User.Identity.Name;
                    userMenu.ModifiedDate = DateTime.Now;
                    await _db.SaveChangesAsync();
                    result = userMenu.CompanyMenuId;
                    dbTran.Commit();
                }
                catch (DbEntityValidationException ex)
                {
                    dbTran.Rollback();
                    //throw;
                }
            }
            return result;
        }
        public async Task<int> UserMenuDelete(int id)
        {
            int result = -1;

            if (id != 0)
            {
                using (DbContextTransaction dbTran = _db.Database.BeginTransaction())
                {
                    try
                    {
                        CompanyMenu userMenu = _db.CompanyMenus.Find(id);
                        var CompanyUserMenuList = await _db.CompanyUserMenus
                           .Where(e => e.CompanyMenuId == userMenu.CompanyMenuId
                           && e.CompanyId == userMenu.CompanyId && e.IsActive == true).ToListAsync();
                        CompanyUserMenuList.ForEach(e => e.IsActive = false);

                        userMenu.IsActive = false;
                        userMenu.ModifiedBy = System.Web.HttpContext.Current.User.Identity.Name;
                        userMenu.ModifiedDate = DateTime.Now;
                        await _db.SaveChangesAsync();
                        result = userMenu.CompanyMenuId;
                        dbTran.Commit();
                    }
                    catch (DbEntityValidationException ex)
                    {
                        dbTran.Rollback();
                        //throw;
                    }
                }
            }
            return result;
        }
        #endregion

        #region User Submenu
        public async Task<VMUserSubMenu> UserSubMenuGet()
        {
            VMUserSubMenu vmUserSubMenu = new VMUserSubMenu();

            vmUserSubMenu.DataList = await Task.Run(() => UserSubMenuDataLoad());

            return vmUserSubMenu;
        }

        public IEnumerable<VMUserSubMenu> UserSubMenuDataLoad()
        {
            var v = (from t1 in _db.CompanySubMenus
                     join t2 in _db.CompanyMenus on t1.CompanyMenuId equals t2.CompanyMenuId
                     join t3 in _db.Companies on t2.CompanyId equals t3.CompanyId

                     where t1.IsActive == true
                     select new VMUserSubMenu
                     {
                         CompanyName = t3.Name,
                         ID = t1.CompanySubMenuId,
                         Name = t1.Name,
                         Param = t1.Param,
                         CompanyFK = t1.CompanyId,
                         Controller = t1.Controller,
                         IsActive = t1.IsActive,
                         LayerNo = t1.LayerNo,
                         ShortName = t1.ShortName,
                         Action = t1.Action,
                         UserMenuName = t2.Name,
                         User_MenuFk = t2.CompanyMenuId,
                         Priority = t1.OrderNo

                     }).OrderByDescending(x => x.ID).AsEnumerable();
            return v;
        }


        public async Task<int> UserSubMenuAdd(VMUserSubMenu vmUserSubMenu)
        {
            var result = -1;

            var objectToSave = await _db.CompanySubMenus
                .SingleOrDefaultAsync(q => q.Name == vmUserSubMenu.Name
                && q.CompanyId == vmUserSubMenu.CompanyFK
                && q.CompanyMenuId == vmUserSubMenu.User_MenuFk
                );

            if (objectToSave != null)
            {

                return result = objectToSave.CompanySubMenuId;
            }


            CompanySubMenu userSubMenu = new CompanySubMenu
            {
                CompanySubMenuId = _db.Database.SqlQuery<int>("exec spGetNewCompanyId").FirstOrDefault(),
                Name = vmUserSubMenu.Name,
                CompanyId = vmUserSubMenu.CompanyFK,
                CompanyMenuId = vmUserSubMenu.User_MenuFk,
                OrderNo = vmUserSubMenu.Priority,
                Controller = vmUserSubMenu.Controller,
                Action = vmUserSubMenu.Action,
                LayerNo = vmUserSubMenu.LayerNo,
                IsActive = true,
                IsSideMenu = true,
                ShortName = vmUserSubMenu.ShortName,
                Param = vmUserSubMenu.Param,
                CreatedBy = System.Web.HttpContext.Current.User.Identity.Name,
                CreatedDate = DateTime.Now



            };
            _db.CompanySubMenus.Add(userSubMenu);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = userSubMenu.CompanySubMenuId;
            }
            return result;
        }
        public async Task<int> UserSubMenuEdit(VMUserSubMenu vmUserSubMenu)
        {
            var result = -1;
            using (DbContextTransaction dbTran = _db.Database.BeginTransaction())
            {
                try
                {
                    CompanySubMenu userSubMenu = _db.CompanySubMenus.Find(vmUserSubMenu.ID);

                    var CompanyUserMenuList = await _db.CompanyUserMenus
                        .Where(e => e.CompanyMenuId == userSubMenu.CompanyMenuId
                        && e.CompanySubMenuId == userSubMenu.CompanySubMenuId).ToListAsync();
                    CompanyUserMenuList.ForEach(e => e.CompanyMenuId = vmUserSubMenu.User_MenuFk);

                    userSubMenu.CompanyMenuId = vmUserSubMenu.User_MenuFk;
                    userSubMenu.Name = vmUserSubMenu.Name;
                    userSubMenu.OrderNo = vmUserSubMenu.Priority;
                    userSubMenu.Controller = vmUserSubMenu.Controller;
                    userSubMenu.Action = vmUserSubMenu.Action;
                    userSubMenu.LayerNo = vmUserSubMenu.LayerNo;
                    userSubMenu.ShortName = vmUserSubMenu.ShortName;
                    userSubMenu.Param = vmUserSubMenu.Param;
                    userSubMenu.CompanyId = vmUserSubMenu.CompanyFK;
                    userSubMenu.ModifiedBy = System.Web.HttpContext.Current.User.Identity.Name;
                    userSubMenu.ModifiedDate = DateTime.Now;
                    _db.Entry(userSubMenu).State = EntityState.Modified;
                    await _db.SaveChangesAsync();
                    result = userSubMenu.CompanySubMenuId;



                    dbTran.Commit();
                }
                catch (DbEntityValidationException ex)
                {
                    dbTran.Rollback();
                    //throw;
                }
            }
            return result;
        }
        //public async Task<int> UserSubMenuEdit(VMUserSubMenu vmUserSubMenu)
        //{

        //    var result = -1;
        //    //to select Accountining_Chart_Two data.....
        //    CompanySubMenu userSubMenu = _db.CompanySubMenus.Find(vmUserSubMenu.ID);
        //    userSubMenu.CompanyMenuId = vmUserSubMenu.User_MenuFk;
        //    userSubMenu.Name = vmUserSubMenu.Name;
        //    userSubMenu.OrderNo = vmUserSubMenu.Priority;
        //    userSubMenu.Controller = vmUserSubMenu.Controller;
        //    userSubMenu.Action = vmUserSubMenu.Action;
        //    userSubMenu.LayerNo = vmUserSubMenu.LayerNo;
        //    userSubMenu.ShortName = vmUserSubMenu.ShortName;
        //    userSubMenu.Param = vmUserSubMenu.Param;
        //    userSubMenu.CompanyId = vmUserSubMenu.CompanyFK;
        //    userSubMenu.ModifiedBy = System.Web.HttpContext.Current.User.Identity.Name;
        //    userSubMenu.ModifiedDate = DateTime.Now;
        //    _db.Entry(userSubMenu).State = EntityState.Modified;
        //    if (await _db.SaveChangesAsync() > 0)
        //    {
        //        result = userSubMenu.CompanySubMenuId;
        //    }
        //    return result;
        //}

        public async Task<int> UserSubMenuDelete(int id)
        {
            int result = -1;

            if (id != 0)
            {
                using (DbContextTransaction dbTran = _db.Database.BeginTransaction())
                {
                    try
                    {
                        CompanySubMenu userSubMenu = _db.CompanySubMenus.Find(id);

                        var CompanyUserMenuList = await _db.CompanyUserMenus
                            .Where(e => e.CompanyMenuId == userSubMenu.CompanyMenuId
                            && e.CompanySubMenuId == userSubMenu.CompanySubMenuId && e.IsActive == true).ToListAsync();
                        CompanyUserMenuList.ForEach(e => e.IsActive = false);

                        userSubMenu.IsActive = false;
                        userSubMenu.ModifiedBy = System.Web.HttpContext.Current.User.Identity.Name;
                        userSubMenu.ModifiedDate = DateTime.Now;
                        _db.Entry(userSubMenu).State = EntityState.Modified;
                        result = await _db.SaveChangesAsync();
                        dbTran.Commit();
                    }
                    catch (DbEntityValidationException ex)
                    {
                        dbTran.Rollback();
                        //throw;
                    }
                }
            }
            return result;
        }
        #endregion

       
        public class EnumModel
        {
            public int Value { get; set; }
            public string Text { get; set; }
        }
        public List<object> CommonCountriesDropDownList()
        {
            var list = new List<object>();
          
            return list;
        }
        public List<object> CommonDistrictsDropDownList()
        {
            var list = new List<object>();
            var v = _db.Districts.Where(a => a.IsActive).ToList();
            foreach (var x in v)
            {
                list.Add(new { Text = x.Name, Value = x.DistrictId });
            }
            return list;
        }
        public List<object> CommonUpazilasDropDownList()
        {
            var list = new List<object>();
            var v = _db.Upazilas.Where(a => a.IsActive == true).ToList();
            foreach (var x in v)
            {
                list.Add(new { Text = x.Name, Value = x.UpazilaId });
            }
            return list;
        }
        public List<object> CommonDivisionsDropDownList()
        {
            var list = new List<object>();
            var v = _db.Divisions.ToList();
            foreach (var x in v)
            {
                list.Add(new { Text = x.Name, Value = x.DivisionId });
            }
            return list;
        }
        public class CustomerPaymentType
        {
            public string Text { get; set; }
            public string Value { get; set; }

        }
        public List<object> CommonCustomerPaymentType()
        {
            var list = new List<object>();

            var students = new List<CustomerPaymentType>() {
                new CustomerPaymentType(){ Text = "Credit", Value="Credit"},
                new CustomerPaymentType(){ Text = "Cash", Value="Cash"},
                new CustomerPaymentType(){ Text = "Special", Value="Special"}


            };

            foreach (var x in students)
            {
                list.Add(new { Text = x.Text, Value = x.Value });
            }
            return list;
        }

        public List<object> CommonRelationList()
        {
            var list = new List<object>();
            list.Add(new { Text = "Wife", Value = "Wife" });
            list.Add(new { Text = "Husband", Value = "Husband" });
            list.Add(new { Text = "Son", Value = "Son" });
            list.Add(new { Text = "Doughter", Value = "Doughter" });
            list.Add(new { Text = "Brother", Value = "Brother" });
            list.Add(new { Text = "Sister", Value = "Sister" });
            list.Add(new { Text = "Father", Value = "Father" });
            list.Add(new { Text = "Mother", Value = "Mother" });

            return list;
        }
        public List<object> CommonZonesDropDownList(int companyId)
        {
            var list = new List<object>();
           
            return list;
        }
        public List<SelectModelType> GCClZoneDropDownList(int companyId)
        {
            var list = new List<SelectModelType>();
           
            return list;
        }
        public List<object> CompaniesDropDownList()
        {
            var list = new List<object>();
            var v = _db.Companies.ToList();
            foreach (var x in v)
            {
                list.Add(new { Text = x.Name, Value = x.CompanyId });
            }
            return list;
        }
        public List<object> CompanyMenusDropDownList()
        {
            var list = new List<object>();
            var v = _db.CompanyMenus.ToList();
            foreach (var x in v)
            {
                list.Add(new { Text = x.Name, Value = x.CompanyMenuId });
            }
            return list;
        }
        public List<object> CommonSubZonesDropDownList(int companyId, int zoneId = 0)
        {
            var list = new List<object>();
            
            return list;
        }

        public async Task<VMAccountingSignatory> GetAccountingSignatory(int companyId)
        {
            VMAccountingSignatory vmAccountingSignatory = new VMAccountingSignatory();
            vmAccountingSignatory.CompanyFK = companyId;
            vmAccountingSignatory.DataList = await Task.Run(() => (from t1 in _db.Accounting_Signatory
                                                                   join t2 in _db.Companies on t1.CompanyId equals t2.CompanyId
                                                                   where t1.CompanyId == companyId && t1.IsActive

                                                                   select new VMAccountingSignatory
                                                                   {
                                                                       SignatoryId = t1.SignatoryId,
                                                                       SignatoryType = t1.SignatoryType,
                                                                       SignatoryName = t1.SignatoryName,
                                                                       CompanyName = t2.Name,
                                                                       OrderBy = t1.OrderBy,
                                                                       CompanyFK = t1.CompanyId
                                                                   }).OrderByDescending(x => x.SignatoryId).AsEnumerable());
            return vmAccountingSignatory;
        }

        public async Task<int> AccountingSignatoryAdd(VMAccountingSignatory vmAccountingSignatory)
        {
            var result = -1;
            Accounting_Signatory commo = new Accounting_Signatory
            {

                SignatoryName = vmAccountingSignatory.SignatoryName,
                SignatoryType = vmAccountingSignatory.SignatoryType,
                OrderBy = vmAccountingSignatory.OrderBy,
                CompanyId = vmAccountingSignatory.CompanyFK.Value,
                CreatedBy = System.Web.HttpContext.Current.User.Identity.Name,
                CreatedDate = DateTime.Now,
                IsActive = true,
                Priority = 1
            };
            _db.Accounting_Signatory.Add(commo);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = commo.SignatoryId;
            }
            return result;
        }
        public async Task<int> AccountingSignatoryEdit(VMAccountingSignatory vmAccountingSignatory)
        {

            var result = -1;
            Accounting_Signatory accountingSignatory = _db.Accounting_Signatory.Find(vmAccountingSignatory.SignatoryId);
            accountingSignatory.CompanyId = vmAccountingSignatory.CompanyFK.Value;
            accountingSignatory.SignatoryType = vmAccountingSignatory.SignatoryType;
            accountingSignatory.OrderBy = vmAccountingSignatory.OrderBy;

            accountingSignatory.SignatoryName = vmAccountingSignatory.SignatoryName;
            accountingSignatory.ModifiedBy = System.Web.HttpContext.Current.User.Identity.Name;
            accountingSignatory.ModifiedDate = DateTime.Now;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = accountingSignatory.SignatoryId;
            }
            return result;
        }
        public async Task<int> AccountingSignatoryDelete(int id)
        {
            int result = -1;

            if (id != 0)
            {
                Accounting_Signatory accountingSignatory = _db.Accounting_Signatory.Find(id);
                accountingSignatory.IsActive = false;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = accountingSignatory.SignatoryId;
                }
            }
            return result;
        }


        public List<object> CommonActSignatorysDropDownList(int companyId)
        {
            var list = new List<object>();
            var v = _db.Accounting_Signatory.Where(x => x.CompanyId == companyId).ToList();
            foreach (var x in v)
            {
                list.Add(new { Text = x.SignatoryName, Value = x.SignatoryId });
            }
            return list;
        }
      
        //New Compamy Setup
        public async Task<VMCompany> GetCompany()
        {
            VMCompany VMCompany = new VMCompany();

            VMCompany.DataList = await Task.Run(() => (from x in _db.Companies


                                                       select new VMCompany
                                                       {
                                                           ID = x.CompanyId,
                                                           Name = x.Name,
                                                           ShortName = x.ShortName,
                                                           OrderNo = x.OrderNo,
                                                           MushokNo = x.MushokNo,
                                                           IsCompany = x.IsCompany
                                                           //CompanyLogo = string.Format("{0}://{1}", HttpContext.Request.Url.Scheme, HttpContext.Request.Url.Authority) + "/Images/Logo/" + (string.IsNullOrEmpty(x.CompanyLogo) ? "logo.png" : x.CompanyLogo),
                                                       }));
            return VMCompany;
        }

        public async Task<int> CompanyAdd(VMCompany VMCompany)
        {

            var result = -1;
            //if (!string.IsNullOrEmpty(VMCompany.CompanyLogo))
            //{
            //   commoLogo = VMCompany.CompanyLogo;
            //}
            Company commo = new Company
            {
                CompanyId = _db.Database.SqlQuery<int>("exec spGetNewCompanyId").FirstOrDefault(),
                CreatedBy = System.Web.HttpContext.Current.User.Identity.Name,
                CreatedDate = DateTime.Now,

                Name = VMCompany.Name,
                ShortName = VMCompany.ShortName,
                OrderNo = VMCompany.OrderNo,
                MushokNo = VMCompany.MushokNo,
                Address = VMCompany.Address,
                Phone = VMCompany.Phone,
                Fax = VMCompany.Fax,
                Email = VMCompany.Email,
                //CompanyLogo = commoLogo,
                IsCompany = VMCompany.IsCompany,
                IsActive = VMCompany.IsActive,

            };
            //SignatoryName = VMCompany.SignatoryName,
            //    SignatoryType = VMCompany.SignatoryType,
            //    CompanyId = VMCompany.CompanyFK.Value,
            //    CreatedBy = System.Web.HttpContext.Current.User.Identity.Name,
            //    CreatedDate = DateTime.Now,
            //    IsActive = true,
            //    Priority = 1

            _db.Companies.Add(commo);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = commo.CompanyId;
            }
            return result;
        }
     


        private string GenerateHead4AccCode(int Head3Id)
        {
            var Head4 = _db.Head3.Where(x => x.Id == Head3Id).FirstOrDefault();


            var Head5DataList = _db.Head4.Where(x => x.ParentId == Head3Id).AsEnumerable();

            string newAccountCode = "";
            if (Head5DataList.Count() > 0)
            {
                string lastAccCode = Head5DataList.OrderByDescending(x => x.AccCode).FirstOrDefault().AccCode;
                string parentPart = lastAccCode.Substring(0, 4);
                string childPart = lastAccCode.Substring(4, 3);
                newAccountCode = parentPart + (Convert.ToInt32(childPart) + 1).ToString().PadLeft(3, '0');

            }
            else
            {
                newAccountCode = Head4.AccCode + "001";

            }
            return newAccountCode;
        }


        private string GenerateHead5AccCode(int Head4Id)
        {
            var Head4 = _db.Head4.Where(x => x.Id == Head4Id).FirstOrDefault();


            var Head5DataList = _db.Head5.Where(x => x.ParentId == Head4Id).AsEnumerable();

            string newAccountCode = "";
            if (Head5DataList.Count() > 0)
            {
                string lastAccCode = Head5DataList.OrderByDescending(x => x.AccCode).FirstOrDefault().AccCode;
                string parentPart = lastAccCode.Substring(0, 7);
                string childPart = lastAccCode.Substring(7, 3);
                newAccountCode = parentPart + (Convert.ToInt32(childPart) + 1).ToString().PadLeft(3, '0');

            }
            else
            {
                newAccountCode = Head4.AccCode + "001";

            }
            return newAccountCode;
        }
        private string GenerateHeadGlAccCode(int Head5Id)
        {
            var Head5 = _db.Head5.Where(x => x.Id == Head5Id).FirstOrDefault();


            var HeadGlDataList = _db.HeadGLs.Where(x => x.ParentId == Head5Id).AsEnumerable();

            string newAccountCode = "";
            if (HeadGlDataList.Count() > 0)
            {
                string lastAccCode = HeadGlDataList.OrderByDescending(x => x.AccCode).FirstOrDefault().AccCode;
                string parentPart = lastAccCode.Substring(0, 10);
                string childPart = lastAccCode.Substring(10, 3);
                newAccountCode = parentPart + (Convert.ToInt32(childPart) + 1).ToString().PadLeft(3, '0');

            }
            else
            {
                newAccountCode = Head5.AccCode + "001";

            }
            return newAccountCode;
        }

    }



}


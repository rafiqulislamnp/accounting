﻿using KGERP.Data.CustomModel;
using KGERP.Data.Models;
using KGERP.Service.ServiceModel;

using KGERP.Utility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace KGERP.Service.Configuration
{
    public abstract class BaseVM
    {

        public int ID { get; set; }
        public string UserId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public ActionEnum ActionEum { get { return (ActionEnum)this.ActionId; } }
        public int ActionId { get; set; } = 1;
        public JournalEnum JournalEnum { get { return (JournalEnum)this.JournalType; } }
        public int JournalType { get; set; } = (int)JournalEnum.JournalVoucer;
        public bool IsActive { get; set; } = true;
        public int? CompanyFK { get; set; }
        public int OrderNo { get; set; }
        public string Remarks { get; set; }
        public string Code { get; set; }
        public string CompanyName { get; set; }
        //public string Message { get; set; }
        //public bool HasError { get; set; }

    }
    public partial class VMAccountingSignatory : BaseVM
    {

        public int SignatoryId { get; set; }
        public string SignatoryName { get; set; }
        public string SignatoryType { get; set; }
        public int OrderBy { get; set; }
        public int Priority { get; set; }
        public IEnumerable<VMAccountingSignatory> DataList { get; set; }
        public SelectList CompanyList { get; set; } = new SelectList(new List<object>());
    }
    public class VMCompany : BaseVM
    {
        public int CompanyId { get; set; }
        public int? ParentId { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        
        public int? CompanyType { get; set; }
        public string MushokNo { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public int? LayerNo { get; set; }
        public string CompanyLogo { get; set; }
        public bool IsCompany { get; set; }
        public IEnumerable<VMCompany> DataList { get; set; }
        public HttpPostedFileBase CompanyLogoUpload { get; set; }

        public string Controller { get; set; }
        public string Action { get; set; }
        public string Param { get; set; }

    }

    public class VMUserMenuAssignment : BaseVM
    {
        //public bool IsAllowed { get; set; }
        public string MenuName { get; set; }
        public int MenuID { get; set; }
        public string SubmenuName { get; set; }
        public int SubmenuID { get; set; }
        public string Title { get; set; }
       
        public string Method { get; set; }
       
        public long CompanyUserMenusId { get; set; }
        public SelectList CompanyList { get; set; } = new SelectList(new List<object>());
        public IEnumerable<VMUserMenuAssignment> DataList { get; set; }
        public int MenuPriority { get; set; }
    }

    public class VMUserMenu : BaseVM
    {
       
        public string Name { get; set; }
        public int Priority { get; set; }
        public IEnumerable<VMUserMenu> DataList { get; set; }
        public int? LayerNo { get; set; }
        public string ShortName { get; set; }
        public SelectList CompanyList { get; set; } = new SelectList(new List<object>());

    }
    public class VMUserSubMenu : BaseVM
    {
       
        public string Name { get; set; }
        public string UserMenuName { get; set; }
        public int User_MenuFk { get; set; }
        public int Priority { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public int? LayerNo { get; set; }
        public string ShortName { get; set; }
        public string Param { get; set; }
        public SelectList UserMenuList { get; set; } = new SelectList(new List<object>());
        public SelectList CompanyList { get; set; } = new SelectList(new List<object>());

        public string DisplayMessage { get; set; }

        public IEnumerable<VMUserSubMenu> DataList { get; set; }

    }
    public class VMPOTremsAndConditions : BaseVM
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public IEnumerable<VMPOTremsAndConditions> DataList { get; set; }
    }

}
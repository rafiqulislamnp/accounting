﻿using AutoMapper;
using KGERP.Data.Models;
using KGERP.Service.ServiceModel;

namespace KGERP.Models
{
    public partial class ModelMapper
    {
        public static void SetUp()
        {
            Mapper.Initialize(cfg =>
            {
               
                cfg.CreateMap<Company, CompanyModel>();
                cfg.CreateMap<CompanyMenu, CompanyMenuModel>();
                cfg.CreateMap<CompanySubMenu, CompanySubMenuModel>();
                cfg.CreateMap<CompanyUserMenu, CompanyUserMenuModel>();             
                cfg.CreateMap<DropDownType, DropDownTypeModel>();
                cfg.CreateMap<DropDownItem, DropDownItemModel>();               
                cfg.CreateMap<CompanyModel, Company>();
                cfg.CreateMap<CompanyMenuModel, CompanyMenu>()
                  .ForMember(d => d.Company, opt => opt.MapFrom(s => s.Company));
                cfg.CreateMap<CompanySubMenuModel, CompanySubMenu>();
                cfg.CreateMap<CompanyUserMenuModel, CompanyUserMenu>();
                cfg.CreateMap<VoucherModel, Voucher>();             
                cfg.CreateMap<DropDownTypeModel, DropDownType>();
                cfg.CreateMap<DropDownItemModel, DropDownItem>();               
                cfg.CreateMap<UserModel, User>();             
                cfg.CreateMap<Head1Model, Head1>();


            });
        }
    }
}



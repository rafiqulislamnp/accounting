﻿using KGERP.Data.Models;
using KGERP.Service.Implementation.AutoComplete;
using KGERP.Utility;
using Ninject.Infrastructure.Language;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KGERP.Controllers.AutoComplete
{
    public class AutoCompleteController : Controller
    {
        // GET: AutoComplete
        private readonly IAutoCompleteService _autocompleteService;
        public AutoCompleteController(IAutoCompleteService autoCompleteService)
        {
            _autocompleteService = autoCompleteService;
        }
        public ActionResult GetAllCompanyAutoComplete(string prefix=null)
        {
            var companyList =  _autocompleteService.GetAllCompanyAutoCompleteAsync(prefix).Result.ToEnumerable();
            return Json(companyList,JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetAllProjectList(int? companyId)
        {
            var companyList = _autocompleteService.GetAllProjectList(companyId).Result.ToEnumerable();
            return Json(companyList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetAllEmployeeAutoComplete(string prefix = null)
        {
            var companyList = _autocompleteService.GetAllEmployeeAutoComplete(prefix).Result.ToEnumerable();
            return Json(companyList, JsonRequestBehavior.AllowGet);
        }
       
    }
}
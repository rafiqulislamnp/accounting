﻿using DocumentFormat.OpenXml.EMMA;
using KGERP.Data.Models;
using KGERP.Service.Configuration;
using KGERP.Service.Implementation;
using KGERP.Service.Implementation.CurrencyCon;

using KGERP.Service.Interface;

using KGERP.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Pos.App.Controllers
{
    [SessionExpire]
    public class ConfigurationController : Controller
    {

        private HttpContext httpContext;
        private readonly ConfigurationService _service;
        private readonly ICompanyService _companyService;
     
        private readonly IVoucherTypeService voucherTypeService;
       
        public ConfigurationController(
            ICompanyService companyService,
            IVoucherTypeService voucherTypeService,
            ConfigurationService configurationService
            )
        {
            _service = configurationService;
            _companyService = companyService;
           
            this.voucherTypeService = voucherTypeService;
           
        }

        #region User Role Menuitem
        public async Task<ActionResult> UserMenuAssignment(int companyId)
        {
            VMUserMenuAssignment vmUserMenuAssignment = new VMUserMenuAssignment();
            vmUserMenuAssignment.CompanyList = new SelectList(_service.CompaniesDropDownList(), "Value", "Text");

            return View(vmUserMenuAssignment);
        }
        [HttpPost]
        public async Task<ActionResult> UserMenuAssignment(VMUserMenuAssignment model)
        {
            VMUserMenuAssignment vmUserMenuAssignment = new VMUserMenuAssignment();
            vmUserMenuAssignment = await _service.UserMenuAssignmentGet(model);
            return View(vmUserMenuAssignment);
        }

        public JsonResult CompanyUserMenuEdit(int id, bool isActive)
        {
            VMUserMenuAssignment model = new VMUserMenuAssignment
            {
                IsActive = isActive,
                CompanyUserMenusId = id
            };
            CompanyUserMenu companyUserMenu = _service.CompanyUserMenuEdit(model);
            return Json(new { menuid = companyUserMenu.CompanyUserMenuId, updatedstatus = companyUserMenu.IsActive });
        }

        #endregion
        public async Task<ActionResult> AccountingCostCenter(int companyId)
        {
            VMUserMenu vmUserMenu;
            vmUserMenu = await Task.Run(() => _service.AccountingCostCenterGet(companyId));

            return View(vmUserMenu);
        }
        [HttpPost]
        public async Task<ActionResult> AccountingCostCenter(VMUserMenu model)
        {

            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.AccountingCostCenterAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.AccountingCostCenterEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.AccountingCostCenterDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction(nameof(AccountingCostCenter), new { companyId = model.CompanyFK });
        }

        #region User Menu
        public async Task<ActionResult> UserMenu()
        {
            VMUserMenu vmUserMenu;
            vmUserMenu = await Task.Run(() => _service.UserMenuGet());
            vmUserMenu.CompanyList = new SelectList(_service.CompaniesDropDownList(), "Value", "Text");

            return View(vmUserMenu);
        }
        [HttpPost]
        public async Task<ActionResult> UserMenu(VMUserMenu model)
        {

            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.UserMenuAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.UserMenuEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.UserMenuDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction(nameof(UserMenu), new { companyId = model.CompanyFK });
        }


        #endregion

        #region User Submenu
        public async Task<ActionResult> UserSubMenu()
        {
            VMUserSubMenu vmUserSubMenu;
            vmUserSubMenu = await Task.Run(() => _service.UserSubMenuGet());
            vmUserSubMenu.UserMenuList = new SelectList(_service.CompanyMenusDropDownList(), "Value", "Text");
            vmUserSubMenu.CompanyList = new SelectList(_service.CompaniesDropDownList(), "Value", "Text");

            return View(vmUserSubMenu);
        }
        [HttpPost]
        public async Task<ActionResult> UserSubMenu(VMUserSubMenu model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                await _service.UserSubMenuAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                await _service.UserSubMenuEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                await _service.UserSubMenuDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction(nameof(UserSubMenu), new { companyId = model.CompanyFK });
        }
        #endregion

      

        public async Task<ActionResult> AccountingSignatory(int companyId)
        {

            VMAccountingSignatory vmAccountingSignatory = new VMAccountingSignatory();
            vmAccountingSignatory = await Task.Run(() => _service.GetAccountingSignatory(companyId));
            vmAccountingSignatory.CompanyList = new SelectList(_service.CompaniesDropDownList(), "Value", "Text");
            return View(vmAccountingSignatory);
        }
        [HttpPost]
        public async Task<ActionResult> AccountingSignatory(VMAccountingSignatory vmAccountingSignatory)
        {

            if (vmAccountingSignatory.ActionEum == ActionEnum.Add)
            {


                await _service.AccountingSignatoryAdd(vmAccountingSignatory);
            }
            else if (vmAccountingSignatory.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.AccountingSignatoryEdit(vmAccountingSignatory);
            }
            else if (vmAccountingSignatory.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.AccountingSignatoryDelete(vmAccountingSignatory.SignatoryId);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction(nameof(AccountingSignatory), new { companyId = vmAccountingSignatory.CompanyFK });
        }


        public async Task<ActionResult> Company()
        {

            VMCompany VMCompany = new VMCompany();
            VMCompany = await Task.Run(() => _service.GetCompany());
            //VMCompany.CompanyList = new SelectList(_service.CompaniesDropDownList(), "Value", "Text");

            return View(VMCompany);
        }
        [HttpPost]
        public async Task<ActionResult> Company(VMCompany VMCompany)
        {

            if (VMCompany.ActionEum == ActionEnum.Add)
            {


                await _service.CompanyAdd(VMCompany);
            }
            //else if (VMCompany.ActionEum == ActionEnum.Edit)
            //{
            //    //Edit
            //    await _service.AccountingSignatoryEdit(VMCompany);
            //}
            //else if (VMCompany.ActionEum == ActionEnum.Delete)
            //{
            //    //Delete
            //    await _service.AccountingSignatoryDelete(VMCompany.SignatoryId);
            //}
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction(nameof(Company));
        }


    }
}
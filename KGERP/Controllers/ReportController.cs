using AutoMapper.Mappers;
using DocumentFormat.OpenXml.Drawing.Charts;
using DocumentFormat.OpenXml.EMMA;
using KGERP.Controllers.Custom_Authorization;

using KGERP.Data.CustomModel;
using KGERP.Data.Models;
using KGERP.Service.Implementation;
using KGERP.Service.Implementation.ApprovalSystemService;

using KGERP.Service.Interface;

using KGERP.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security;
using System.Threading.Tasks;
using System.Web.Mvc;
using static KGERP.Utility.Util.PermissionCollection.Crms;

namespace KGERP.Controllers
{
    //[SessionExpire]
    //test azim
    public class ReportController : Controller
    {


        private readonly ICompanyService companyService;
        private readonly IVoucherTypeService voucherTypeService;

        private readonly AccountingService _accountingService;
        private readonly ConfigurationService _configrationService;
        private readonly IApproval_Service _appService;



        string password = "KGerp@321";
        string admin = "Administrator";
        string url = "http://192.168.2.2/ReportServer/?%2fErpReport/";
        public ReportController(ERPEntities db,
              ICompanyService companyService, IVoucherTypeService voucherTypeService,
               ConfigurationService configurationService,
              IApproval_Service _appService)
        {

            this.companyService = companyService;
            this.voucherTypeService = voucherTypeService;

            this._appService = _appService;

            _accountingService = new AccountingService(db);
            _configrationService = configurationService;

        }

        [HttpGet]
        [SessionExpire]
        public ActionResult Index()
        {
            return View();
        }


        // GET: Report
        [HttpGet]

        public ActionResult GetEmployeeReport(string employeeId)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = url + "Employee&rs:Command=Render&rs:Format=PDF&EmployeeId=" + employeeId;
            return File(client.DownloadData(reportURL), "application/pdf");
        }

        [HttpGet]

        public ActionResult ReportTemplate(int companyId, string reportName, string reportDescription)
        {
            var rptInfo = new ReportInfo
            {
                ReportName = reportName,
                ReportDescription = reportDescription,
                ReportURL = String.Format("../../Reports/ReportTemplate.aspx?ReportName={0}&ReportDescription={1}&Height={2}", reportName, reportDescription, 650),
                Width = 100,
                Height = 650
            };
            return View(rptInfo);
        }


        [HttpGet]

        public ActionResult CRReportTemplate(int companyId, string reportName, string reportDescription)
        {
            var rptInfo = new ReportInfo
            {
                ReportName = reportName,
                ReportDescription = reportDescription,
                ReportURL = String.Format("../../Reports/ReportTemplate.aspx?ReportName={0}&ReportDescription={1}&Height={2}&companyId={3}", reportName, reportDescription, 650, companyId),
                Width = 100,
                Height = 650
            };
            return View(rptInfo);
        }
        // GET: Report
        [HttpGet]

        public ActionResult GetOrderInvoiceReport(string orderMasterId)
        {
            var companyId = Convert.ToInt32(Request.QueryString["companyId"]);
            string reportURL;
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            if (companyId == (int)CompanyName.KrishibidFarmMachineryAndAutomobilesLimited)
            {
                reportURL = url + "KFMALOrderInvoice&rs:Command=Render&rs:Format=PDF&OrderMasterId=" + orderMasterId;
            }

            else
            {
                reportURL = url + "OrderInvoice&rs:Command=Render&rs:Format=PDF&OrderMasterId=" + orderMasterId;
            }


            return File(client.DownloadData(reportURL), "application/pdf");
        }

        [HttpGet]

        public ActionResult GetEmiReport(int emiId)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = url + "EmiReportForKFMAL&rs:Command=Render&rs:Format=PDF&EmiId=" + emiId;
            return File(client.DownloadData(reportURL), "application/pdf");
        }

        [HttpGet]

        public ActionResult GetKgeComOrderInvoiceReport(string orderMasterId)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = url + "KGeComInvoiceReport&rs:Command=Render&rs:Format=PDF&OrderMasterId=" + orderMasterId;
            return File(client.DownloadData(reportURL), "application/pdf");
        }

        [HttpGet]

        public ActionResult GetKFMALCOstingReport(int storeId)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = url + "CogsCostingReport&rs:Command=Render&rs:Format=PDF&StoreId=" + storeId;
            return File(client.DownloadData(reportURL), "application/pdf");
        }

        //[HttpGet]
        // 
        //public ActionResult GetStockReport()
        //{
        //    NetworkCredential nwc = new NetworkCredential(admin, AdminPassword);
        //    WebClient client = new WebClient();
        //    client.Credentials = nwc;
        //    string reportURL =url + "StockReport&rs:Command=Render&rs:Format=PDF";
        //    //string reportURL = url + "StockReport" ;
        //    return File(client.DownloadData(reportURL), "application/pdf");
        //}

        [HttpGet]

        public ActionResult GetStockReport(int companyId, string reportName)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);



            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format=PDF&CompanyId={1}", reportName, companyId);
            return File(client.DownloadData(reportURL), "application/pdf");
        }

        [HttpGet]

        public ActionResult GetEcomStockReport()
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = url + "KGeComCurrentStock&rs:Command=Render&rs:Format=PDF";
            //string reportURL = url + "StockReport" ;
            return File(client.DownloadData(reportURL), "application/pdf");
        }

        public ActionResult GetRMStockReport()
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = url + "RmStockReport&rs:Command=Render&rs:Format=PDF";
            //string reportURL = url + "StockReport" ;
            return File(client.DownloadData(reportURL), "application/pdf");
        }

        public ActionResult GetRMDeliverReport(int requisitionId)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = url + "RMDeliveryReport&rs:Command=Render&rs:Format=PDF&RequisitionId=" + requisitionId;
            //string reportURL = url + "StockReport" ;
            return File(client.DownloadData(reportURL), "application/pdf");
        }

        public ActionResult GetRequisitionReport(int requisitionId)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = url + "RequisitionReport&rs:Command=Render&rs:Format=PDF&RequisitionId=" + requisitionId;
            //string reportURL = url + "StockReport" ;
            return File(client.DownloadData(reportURL), "application/pdf");
        }

        [HttpGet]

        public ActionResult GetDeliveryInvoiceReport(long OrderDeliverId, int companyId)
        {
            string reportName = string.Empty;
            if (companyId == 8)
            {
                reportName = "DeliveryInvoice";
            }

            //else if (companyId == 29)
            //{
            //    reportName = "GloryFeedDeliveryInvoice";
            //}
            //else if (companyId == (int)CompanyName.KrishibidFarmMachineryAndAutomobilesLimited)
            //{
            //    reportName = "KFMALDeliveryInvoice";
            //}
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format=PDF&OrderDeliverId={1}", reportName, OrderDeliverId);
            return File(client.DownloadData(reportURL), "application/pdf");
        }


        [HttpGet]

        public ActionResult FeedMYIncentivePolicy(int companyId)
        {
            string reportName = string.Empty;
            if (companyId == 8)
            {
                reportName = "FeedMYIncentivePolicy";
            }
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format=PDF&CompanyId={1}", reportName, companyId);
            return File(client.DownloadData(reportURL), "application/pdf");
        }

        [HttpGet]

        public ActionResult GetDeliveryChallanReport(long orderMasterId, int companyId)
        {
            string reportName = string.Empty;

            if (companyId == 8)
            {
                reportName = "DeliveryChallan";
            }
            else if (companyId == 29)
            {
                reportName = "GloryFeedDeliveryInvoice";
            }
            else if (companyId == (int)CompanyName.KrishibidFarmMachineryAndAutomobilesLimited)
            {
                reportName = "KFMALDeliveryChallan";
            }
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format=PDF&OrderMasterId={1}", reportName, orderMasterId);
            return File(client.DownloadData(reportURL), "application/pdf");
        }

        // GET: Report
        [HttpGet]

        public ActionResult GetPreviousEmployeeReport(long id, string reportName)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format=PDF&Id={1}", reportName, id);
            return File(client.DownloadData(reportURL), "application/pdf");
        }

        // GET: Report
        [HttpGet]

        public ActionResult GetCustomerLedgerReport(int id, string reportName)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format=PDF&VendorId={1}", reportName, id);
            return File(client.DownloadData(reportURL), "application/pdf");
        }

        // GET: Report
        [HttpGet]
        // 
        public ActionResult GetVoucherReport(int companyId, long voucherId, string reportName)
        {
            if (companyId == (int)CompanyName.KrishibidSeedLimited)
            {
                reportName = "KGVoucherReportSeed";

            }
            else
            {
                reportName = "KGVoucherReport";

            }

            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format=PDF&CompanyId={1}&VoucherId={2}", reportName, companyId, voucherId);
            return File(client.DownloadData(reportURL), "application/pdf");
        }
        [HttpGet]
        public ActionResult GetMultiVoucherReport(int companyId)
        {
            string reportName = "KGMultipleVoucherReport";

            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format=PDF&CompanyId={1}", reportName, companyId);
            return File(client.DownloadData(reportURL), "application/pdf");
        }


        // GET: Demand Report
        [HttpGet]

        public ActionResult GetDemandReport(int demandId, string reportName)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format=PDF&DemandId={1}", reportName, demandId);
            return File(client.DownloadData(reportURL), "application/pdf");
        }

        [HttpGet]

        public ActionResult GCCLPurchseOrderReport(int purchaseOrderId, int companyId, string reportName)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format=PDF&CompanyId={1}&PurchaseOrderId={2}", reportName, 24, purchaseOrderId);
            return File(client.DownloadData(reportURL), "application/pdf");
        }
        [HttpGet]

        public ActionResult KFMALLPurchaseOrderReports(int purchaseOrderId, int companyId, string reportName)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format=PDF&CompanyId={1}&PurchaseOrderId={2}", reportName, companyId, purchaseOrderId);
            return File(client.DownloadData(reportURL), "application/pdf");
        }
        [HttpGet]

        public ActionResult GCCLPurchseInvoiceReport(int companyId, int materialReceiveId, string reportName)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format=PDF&CompanyId={1}&materialReceiveId={2}", reportName, companyId, materialReceiveId);
            return File(client.DownloadData(reportURL), "application/pdf", "Purchase Invoice");
        }

        [HttpGet]

        public ActionResult GCCLSalesInvoiceReport(int companyId, int orderMasterId, string reportName)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format=PDF&CompanyId={1}&OrderMasterId={2}", reportName, companyId, orderMasterId);
            return File(client.DownloadData(reportURL), "application/pdf");
        }

        [HttpGet]

        public ActionResult GCCLPRFInvoiceReport(int companyId, int DemandId, string reportName, int CustomerId, string AsOnDate)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            //var fdate = AsOnDate.ToString("dd-MM-yyyy");
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format=PDF&CompanyId={1}&DemandId={2}&CustomerId={3}&AsOnDate={4}", reportName, companyId, DemandId, CustomerId, AsOnDate);
            return File(client.DownloadData(reportURL), "application/pdf");
        }


        [HttpGet]

        public ActionResult GCCLProductionReport(int companyId, int prodReferenceId, string reportName)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format=PDF&CompanyId={1}&ProdReferenceId={2}", reportName, companyId, prodReferenceId);
            return File(client.DownloadData(reportURL), "application/pdf");
        }
        // GET: Purchase Order Report
        [HttpGet]

        public ActionResult GetPurchseOrderReport(int purchaseOrderId, string reportName)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format=PDF&PurchaseOrderId={1}", reportName, purchaseOrderId);
            return File(client.DownloadData(reportURL), "application/pdf");
        }

        // GET: Purchase Order Report
        [HttpGet]

        public ActionResult GetMRRReport(long materialReceiveId, string reportName)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format=PDF&MaterialReceiveId={1}", reportName, materialReceiveId);
            return File(client.DownloadData(reportURL), "application/pdf");
        }

        [HttpGet]

        public ActionResult GetChartOfAccountReport(int companyId, string reportType, string reportName)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&CompanyId={2}", reportName, reportType, companyId);

            if (reportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", "ChartOfAccount.xls");
            }
            if (reportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (reportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", "ChartOfAccount.doc");
            }

            return View();

        }

        [HttpGet]

        public ActionResult CompanyZoneAndTerritoryReport(int companyId, string reportType, string reportName)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&CompanyId={2}", reportName, reportType, companyId);

            if (reportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", "ChartOfAccount.xls");
            }
            if (reportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (reportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", "ChartOfAccount.doc");
            }

            return View();

        }
        [HttpGet]

        public ActionResult ProdReferenceGet(int companyId, string reportType, string reportName)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}", reportName, reportType);

            if (reportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", "ProdReference.xls");
            }
            if (reportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (reportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", "ProdReference.doc");
            }

            return View();

        }



        // GET: General Ledger Report
        [HttpGet]


        public ActionResult GeneralLedger(int companyId)
        {
            ////Session["CompanyId"] = companyId;
            var company = _accountingService.GetCompanyById(companyId);
            ReportCustomModel cm = new ReportCustomModel() { CompanyId = companyId, CompanyName = company.Name + " (" + company.ShortName + ")", FromDate = DateTime.Now, ToDate = DateTime.Now, StrFromDate = DateTime.Now.ToShortDateString(), StrToDate = DateTime.Now.ToShortDateString() };
            return View(cm);
        }

        [HttpGet]

        public ActionResult GeneralLedgerReport(ReportCustomModel model)
        {
            string accCode = model.AccName.Substring(1, 13);
            string reportName = "";
            reportName = "KGGeneralLedger";
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&AccHeadId={2}&StrFromDate={3}&StrToDate={4}&CompanyId={5}", reportName, model.ReportType, model.Id, model.StrFromDate, model.StrToDate, model.CompanyId);

            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", "GeneralLedger.xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", "GeneralLedger.doc");
            }
            return View();
        }
        [HttpGet]

        public ActionResult CustomerGeneralLedger(int companyId, int VendorTypeId)
        {
            ////Session["CompanyId"] = companyId;
            var company = _accountingService.GetCompanyById(companyId);
            ReportCustomModel cm = new ReportCustomModel() { VendorTypeId = VendorTypeId, CompanyId = companyId, CompanyName = company.Name + " (" + company.ShortName + ")", FromDate = DateTime.Now, ToDate = DateTime.Now, StrFromDate = DateTime.Now.ToShortDateString(), StrToDate = DateTime.Now.ToShortDateString() };
            return View(cm);
        }
        [HttpGet]

        public ActionResult CustomerGeneralLedgerReport(ReportCustomModel model)
        {
            string reportName = "";
            if (model.CompanyId == 10)
            {
                reportName = "KfmalCustomerGeneralLedger";
            }
            else if (model.CompanyId == 24)
            {
                reportName = "GcclCustomerGeneralLedger";
            }


            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&AccHeadId={2}&StrFromDate={3}&StrToDate={4}&CompanyId={5}", reportName, model.ReportType, model.Id, model.StrFromDate, model.StrToDate, model.CompanyId);

            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", "GeneralLedger.xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", "GeneralLedger.doc");
            }
            return View();
        }

        [HttpGet]

        public ActionResult SupplierGeneralLedger(int companyId, int VendorTypeId)
        {
            //Session["CompanyId"] = companyId;
            var company = _accountingService.GetCompanyById(companyId);
            ReportCustomModel cm = new ReportCustomModel() { VendorTypeId = VendorTypeId, CompanyId = companyId, CompanyName = company.Name + " (" + company.ShortName + ")", FromDate = DateTime.Now, ToDate = DateTime.Now, StrFromDate = DateTime.Now.ToShortDateString(), StrToDate = DateTime.Now.ToShortDateString() };
            return View(cm);
        }
        [HttpGet]

        public ActionResult SupplierGeneralLedgerReport(ReportCustomModel model)
        {
            string reportName = "";
            if (model.CompanyId == 10)
            {
                reportName = "KfmalSupplierGeneralLedger";
            }
            else if (model.CompanyId == 24)
            {
                reportName = "GcclSupplierGeneralLedger";
            }


            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&AccHeadId={2}&StrFromDate={3}&StrToDate={4}&CompanyId={5}", reportName, model.ReportType, model.Id, model.StrFromDate, model.StrToDate, model.CompanyId);

            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", "GeneralLedger.xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", "GeneralLedger.doc");
            }
            return View();
        }

        // GET: Shareholder General Ledger
        [HttpGet]

        public ActionResult ShareholderGeneralLedger(int companyId)
        {
            //Session["CompanyId"] = companyId;
            var company = _accountingService.GetCompanyById(companyId);
            ReportCustomModel cm = new ReportCustomModel() { CompanyId = companyId, CompanyName = company.Name + " (" + company.ShortName + ")", FromDate = DateTime.Now, ToDate = DateTime.Now, StrFromDate = DateTime.Now.ToShortDateString(), StrToDate = DateTime.Now.ToShortDateString() };
            return View(cm);

        }

        [HttpGet]

        public ActionResult ShareholderGeneralLedgerReport(ReportCustomModel model)
        {
            string accCode = model.AccName.Substring(1, 13);
            string reportName = "";
            reportName = "ShareholderGeneralLedger";
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&AccHeadId={2}&StrFromDate={3}&StrToDate={4}&CompanyId={5}", reportName, model.ReportType, model.Id, model.StrFromDate, model.StrToDate, model.CompanyId);

            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", "GeneralLedger.xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", "GeneralLedger.doc");
            }
            return View();
        }


        // GET: General Ledger Report
        [HttpGet]

        public ActionResult GeneralBankOrCashBook(int companyId)
        {
            //Session["CompanyId"] = companyId;
            ReportCustomModel cm = new ReportCustomModel()
            {
                CompanyId = companyId,
                FromDate = DateTime.Now,
                ToDate = DateTime.Now,
                StrFromDate = DateTime.Now.ToShortDateString(),
                StrToDate = DateTime.Now.ToShortDateString(),


            };
            cm.BankOrCashParantList = new SelectList(_accountingService.CashAndBankDropDownList(companyId), "Value", "Text");

            // if (companyId == (int)CompanyName.GloriousCropCareLimited
            //     || companyId == (int)CompanyName.KrishibidFirmLimited
            //     || companyId == (int)CompanyName.KrishibidPackagingLimited
            //     || companyId == (int)CompanyName.KrishibidSeedLimited
            //     )
            // {
            // }

            // else if (companyId == (int)CompanyName.KrishibidPoultryLimited)
            // {
            //     cm.BankOrCashParantList = new SelectList(_accountingService.KPLCashAndBankDropDownList(companyId), "Value", "Text");

            // }

            //if (companyId == (int)CompanyName.KrishibidSeedLimited)
            // {
            //     cm.BankOrCashParantList = new SelectList(_accountingService.SeedCashAndBankDropDownList(companyId), "Value", "Text");

            // }

            return View(cm);
        }

        [HttpGet]

        public ActionResult GeneralBankOrCashBookReport(ReportCustomModel model)
        {

            string reportName = "";
            reportName = "KGBankOrCashBook";

            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&AccHeadId={2}&StrFromDate={3}&StrToDate={4}&CompanyId={5}", reportName, model.ReportType, model.Id, model.StrFromDate, model.StrToDate, model.CompanyId);

            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", "BankCashBookSeed.xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", "GeneralLedger.doc");
            }
            return View();
        }



        // GET:Receipt & Payment Statement Report
        [HttpGet]

        public ActionResult ReceiptPaymentStatementReport(int companyId)
        {
            //Session["CompanyId"] = companyId;
            ReportCustomModel cm = new ReportCustomModel()
            {
                CompanyId = companyId,
                FromDate = DateTime.Now,
                ToDate = DateTime.Now,
                StrFromDate = DateTime.Now.ToShortDateString(),
                StrToDate = DateTime.Now.ToShortDateString(),
            };
            return View(cm);
        }

        [HttpGet]

        public ActionResult GetReceiptPaymentStatementReport(ReportCustomModel model)
        {

            string reportName = "";
            reportName = "ReceiptPaymentStatement";
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&StrFromDate={2}&StrToDate={3}&CompanyId={4}", reportName, model.ReportType, model.StrFromDate, model.StrToDate, model.CompanyId);

            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", "ReceiptPaymentStatement.xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", "ReceiptPaymentStatement.doc");
            }
            return View();
        }

        [HttpGet]

        public ActionResult PropertiesReceiptPaymentReport(int companyId)
        {
            //Session["CompanyId"] = companyId;
            ReportCustomModel cm = new ReportCustomModel()
            {
                CompanyId = companyId,
                FromDate = DateTime.Now,
                ToDate = DateTime.Now,
                StrFromDate = DateTime.Now.ToShortDateString(),
                StrToDate = DateTime.Now.ToShortDateString(),
            };
            return View(cm);
        }

        [HttpGet]

        public ActionResult GetPropertiesReceiptPaymentReport(ReportCustomModel model)
        {

            string reportName = "";
            reportName = "PropertiesReceivedPayments";
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&CompanyId={2}&StrFromDate={3}&StrToDate={4}&CostCenterId={5}", reportName, model.ReportType, model.CompanyId, model.StrFromDate, model.StrToDate, model.CostCenterId ?? 0);

            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", "ReceiptPaymentStatement.xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", "ReceiptPaymentStatement.doc");
            }
            return View();
        }

        [HttpGet]

        public ActionResult AccountingMovement(int companyId)
        {
            //Session["CompanyId"] = companyId;
            var company = _accountingService.GetCompanyById(companyId);
            ReportCustomModel cm = new ReportCustomModel()
            {
                CompanyId = companyId,
                FromDate = DateTime.Now,
                ToDate = DateTime.Now,
                CompanyName = company.Name + " (" + company.ShortName + ")",
                StrFromDate = DateTime.Now.ToShortDateString(),
                StrToDate = DateTime.Now.ToShortDateString()
            };
            return View(cm);
        }

        [HttpGet]

        public ActionResult AccountingMovementReports(ReportCustomModel model)
        {
            string reportName = "KGAccountingMovement";
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&AccHeadId={2}&LayerNo={3}&StrFromDate={4}&StrToDate={5}&CompanyId={6}", reportName, model.ReportType, model.HeadGLId, model.LayerNo, model.StrFromDate, model.StrToDate, model.CompanyId);

            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", "BankCashBookSeed.xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", "GeneralLedger.doc");
            }
            return View();
        }


        [HttpGet]

        public ActionResult FeedSalesAndCollection(int companyId)
        {
            //Session["CompanyId"] = companyId;
            var company = _accountingService.GetCompanyById(companyId);
            ReportCustomModel cm = new ReportCustomModel()
            {
                CompanyId = companyId,
                FromDate = DateTime.Now,
                ToDate = DateTime.Now,
                StrFromDate = DateTime.Now.ToShortDateString(),
                StrToDate = DateTime.Now.ToShortDateString()
            };
            return View(cm);
        }

        [HttpGet]

        public ActionResult FeedSalesAndCollectionReports(ReportCustomModel model)
        {

            string reportName = "";
            if (model.CompanyId == (int)CompanyName.KrishibidFeedLimited)
            {

                reportName = "FeedSalesAndCollectionReport";
            }
            else
            {
                reportName = model.ReportName;
            }
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&StrFromDate={2}&StrToDate={3}&CompanyId={4}&Head4Id={5}", reportName, model.ReportType, model.StrFromDate, model.StrToDate, model.CompanyId, model.Head4Id.Value);

            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", "BankCashBookSeed.xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", "GeneralLedger.doc");
            }
            return View();
        }


        [HttpGet]

        public ActionResult GetRawConsumeViaProduction(string StrFromDate, string StrToDate, int CompanyId, int FProductId)
        {
            string reportName = "FeedRMConsumeViaProductionReport";
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&StrFromDate={2}&StrToDate={3}&CompanyId={4}&FProductId={5}", reportName, "PDF", StrFromDate, StrToDate, CompanyId, FProductId);
            return File(client.DownloadData(reportURL), "application/pdf");
        }

        [HttpGet]

        public ActionResult GcclCustomerStatement(int companyId)
        {
            //Session["CompanyId"] = companyId;
            var company = _accountingService.GetCompanyById(companyId);
            ReportCustomModel cm = new ReportCustomModel()
            {
                CompanyId = companyId,
                FromDate = DateTime.Now,
                ToDate = DateTime.Now,
                CompanyName = company.Name + " (" + company.ShortName + ")",
                StrFromDate = DateTime.Now.ToShortDateString(),
                StrToDate = DateTime.Now.ToShortDateString()
            };
            return View(cm);
        }




        [HttpGet]
        public ActionResult AccountingMovementInternal(int HeadGLId, int LayerNo, string StrFromDate, string StrToDate, int CompanyId)
        {
            string reportName = "KGAccountingMovement";

            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&AccHeadId={2}&LayerNo={3}&StrFromDate={4}&StrToDate={5}&CompanyId={6}", reportName, "PDF", HeadGLId, LayerNo, StrFromDate, StrToDate, CompanyId);

            return File(client.DownloadData(reportURL), "application/pdf");

        }

        [HttpGet]

        public ActionResult AccountingAdvancedLedger(int companyId)
        {
            //Session["CompanyId"] = companyId;
            var company = _accountingService.GetCompanyById(companyId);
            ReportCustomModel cm = new ReportCustomModel()
            {
                CompanyId = companyId,
                FromDate = DateTime.Now,
                ToDate = DateTime.Now,
                CompanyName = company.Name + " (" + company.ShortName + ")",
                StrFromDate = DateTime.Now.ToShortDateString(),
                StrToDate = DateTime.Now.ToShortDateString()
            };
            return View(cm);
        }

        [HttpGet]

        public ActionResult AccountingAdvancedLedgerReports(ReportCustomModel model)
        {
            string reportName = "AccountingAdvancedLedger";

            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&AccHeadId={2}&LayerNo={3}&StrFromDate={4}&StrToDate={5}&CompanyId={6}", reportName, model.ReportType, model.HeadGLId, model.LayerNo, model.StrFromDate, model.StrToDate, model.CompanyId);

            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", "BankCashBookSeed.xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", "GeneralLedger.doc");
            }
            return View();
        }

        [HttpGet]

        public ActionResult AccountingAdvancedLedgerReportsInternal(int AccHeadId, int LayerNo, string StrFromDate, string StrToDate, int CompanyId)
        {
            string reportName = "AccountingAdvancedLedger";

            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&AccHeadId={2}&LayerNo={3}&StrFromDate={4}&StrToDate={5}&CompanyId={6}", reportName, "PDF", AccHeadId, LayerNo, StrFromDate, StrToDate, CompanyId);

            return File(client.DownloadData(reportURL), "application/pdf");
            return View();
        }

        [HttpGet]

        public ActionResult AccountingReceivableDetails(int companyId, string reportName)
        {
            //Session["CompanyId"] = companyId;
            var company = _accountingService.GetCompanyById(companyId);
            ReportCustomModel cm = new ReportCustomModel()
            {
                CompanyId = companyId,
                FromDate = DateTime.Now,
                ToDate = DateTime.Now,
                StrFromDate = DateTime.Now.ToShortDateString(),
                StrToDate = DateTime.Now.ToShortDateString(),
                ReportName = reportName
            };
            return View(cm);
        }

        [HttpGet]

        public ActionResult AccountingReceivableDetailsReport(ReportCustomModel model)
        {

            string reportName = "";
            if (model.CompanyId == (int)CompanyName.KrishibidSeedLimited)
            {

                reportName = "AccountingReceivableDetailsSeed";
            }
            else
            {
                reportName = model.ReportName;
            }
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&StrFromDate={2}&StrToDate={3}&CompanyId={4}", reportName, model.ReportType, model.StrFromDate, model.StrToDate, model.CompanyId);

            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", "BankCashBookSeed.xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", "GeneralLedger.doc");
            }
            return View();
        }

        [HttpGet]

        public ActionResult KPLCollectionStatement(int companyId, string reportName, string reportNameDetails)
        {
            //Session["CompanyId"] = companyId;
            ReportCustomModel cm = new ReportCustomModel()
            {
                CompanyId = companyId,
                FromDate = DateTime.Now,
                ToDate = DateTime.Now,
                StrFromDate = DateTime.Now.ToShortDateString(),
                StrToDate = DateTime.Now.ToShortDateString(),
                ReportName = reportName,
                NoteReportName = reportNameDetails,
                CostCenterList = new SelectList(_accountingService.CostCenterDropDownList(companyId), "Value", "Text"),
                VoucherTypeList = new SelectList(_accountingService.VoucherTypesDownList(companyId), "Value", "Text")
            };
            return View(cm);
        }


        [HttpGet]

        public ActionResult KPLCollectionStatementView(ReportCustomModel model)
        {
            string reportName = "";

            reportName = model.ReportName;
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&StrFromDate={2}&StrToDate={3}&CompanyId={4}&CostCenterId={5}&VoucherTypeId={6}", reportName, model.ReportType, model.StrFromDate, model.StrToDate, model.CompanyId, model.CostCenterId ?? 0, model.VoucherTypeId = 0);

            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", "BankCashBookSeed.xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", "GeneralLedger.doc");
            }
            return View();
        }

        [HttpGet]

        public ActionResult CollectionStatement(int companyId, string reportName)
        {
            //Session["CompanyId"] = companyId;
            ReportCustomModel cm = new ReportCustomModel()
            {
                CompanyId = companyId,
                FromDate = DateTime.Now,
                ToDate = DateTime.Now,
                StrFromDate = DateTime.Now.ToShortDateString(),
                StrToDate = DateTime.Now.ToShortDateString(),
                ReportName = reportName,
            };
            return View(cm);
        }

        [HttpGet]

        public ActionResult CollectionStatementView(ReportCustomModel model)
        {
            string reportName = "";

            reportName = model.ReportName;
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&StrFromDate={2}&StrToDate={3}&CompanyId={4}", reportName, model.ReportType, model.StrFromDate, model.StrToDate, model.CompanyId);

            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", "BankCashBookSeed.xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", "GeneralLedger.doc");
            }
            return View();
        }

        [HttpGet]

        public ActionResult CustomerLedger(int companyId, string reportName)
        {
            //Session["CompanyId"] = companyId;
            ReportCustomModel cm = new ReportCustomModel() { CompanyId = companyId, FromDate = DateTime.Now, ToDate = DateTime.Now, StrFromDate = DateTime.Now.ToShortDateString(), StrToDate = DateTime.Now.ToShortDateString(), ReportName = reportName };
            return View(cm);
        }

        [HttpGet]

        public ActionResult CustomerLedgerReport(ReportCustomModel model)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&StrFromDate={2}&StrToDate={3}&CompanyId={4}&VendorId={5}", model.ReportName, model.ReportType, model.StrFromDate, model.StrToDate, model.CompanyId, model.VendorId);

            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", model.ReportName + ".xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", model.ReportName + ".doc");
            }
            return View();
        }

        // GET: Sales Return Report
        [HttpGet]

        public ActionResult GetSalesReturnReport(int saleReturnId, string reportName)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format=PDF&SaleReturnId={1}", reportName, saleReturnId);
            return File(client.DownloadData(reportURL), "application/pdf");
        }

        // GET: Sales Return Report
        [HttpGet]

        public ActionResult GetCustomerReport(int vendorId, int vendorTypeId, string vendorType)
        {
            string reportURL = string.Empty;
            string reportName = string.Empty;
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;

            if (vendorTypeId == 2)
            {
                if (vendorType.Equals("Cash"))
                {
                    reportName = "CashCustomerInformation";
                    reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format=PDF&VendorId={1}", reportName, vendorId);
                }

                if (vendorType.Equals("Credit"))
                {
                    reportName = "CreditCustomerInformation";
                    reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format=PDF&VendorId={1}", reportName, vendorId);
                }

            }
            if (vendorTypeId == 1)
            {
                reportName = "SupplierInformation";
                reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format=PDF&VendorId={1}", reportName, vendorId);
            }

            return File(client.DownloadData(reportURL), "application/pdf");
        }

        // GET: Balance Sheet Report
        [HttpGet]


        public ActionResult BalanceSheet(int companyId, string balanceSheetReportName, string noteReportName)
        {
            ReportCustomModel rcm = new ReportCustomModel()
            {
                CompanyId = companyId,
                FromDate = DateTime.Now,
                StrFromDate = DateTime.Now.ToShortDateString(),
                ReportName = balanceSheetReportName,
                NoteReportName = noteReportName

            };
            return View(rcm);
        }


        [HttpGet]

        public ActionResult GetBalanceSheetReport(ReportCustomModel model)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();

            client.Credentials = nwc;
            long ReportCategoryId = 0;
            string reportURL = "";
            //if (model.CompanyId==28||model.CompanyId==8||model.CompanyId==24||model.CompanyId==20||model.CompanyId==19)
            //{
            //    reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&CompanyId={2}&StrFromDate={3}&CostCenterId={4}&ReportCategoryId={5}", model.ReportName, model.ReportType, model.CompanyId, model.StrFromDate, model.CostCenterId ?? 0, ReportCategoryId);
            //}
            //else
            //{
            //    reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&CompanyId={2}&StrFromDate={3}&CostCenterId={4}", model.ReportName, model.ReportType, model.CompanyId, model.StrFromDate, model.CostCenterId ?? 0);
            //}
            reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&CompanyId={2}&StrFromDate={3}&CostCenterId={4}&ReportCategoryId={5}", model.ReportName, model.ReportType, model.CompanyId, model.StrFromDate, model.CostCenterId ?? 0, ReportCategoryId);

            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", model.ReportName + ".xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", model.ReportName + ".doc");
            }
            return View();
        }

        [HttpGet]
        public ActionResult ApprovalBalanceSheetReport(int companyId, string reportName, int month, int years, int ReportGroup, long ReportCategoryId)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            string reportURL = "";
            client.Credentials = nwc;
            string ReportType = "PDF";
            int CostCenterId = 0;
            var lastDayOfMonth = DateTime.DaysInMonth(years, month);

            string StrFromDate = "01" + "/" + month + "/" + years;
            string StrToDate = lastDayOfMonth + "/" + month + "/" + years;
            if (ReportGroup == 1)
            {
                reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&CompanyId={2}&StrFromDate={3}&CostCenterId={4}&ReportCategoryId={5}", reportName, ReportType, companyId, StrToDate, CostCenterId, ReportCategoryId);
            }
            else if (ReportGroup == 2)
            {
                reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&CompanyId={2}&StrFromDate={3}&StrToDate={4}&CostCenterId={5}&ReportCategoryId={6}", reportName, ReportType, companyId, StrFromDate, StrToDate, CostCenterId, ReportCategoryId);

            }
            return File(client.DownloadData(reportURL), "application/pdf");
        }

        [HttpGet]

        public ActionResult KTTLShareHolderPosition(int companyId, string reportName)
        {
            ReportCustomModel rcm = new ReportCustomModel()
            {
                CompanyId = companyId,
                FromDate = DateTime.Now,
                StrFromDate = DateTime.Now.ToShortDateString(),
                ReportName = reportName
            };
            return View(rcm);
        }

        [HttpGet]

        public ActionResult NFFLShareHolderPositionReport(ReportCustomModel model)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();

            client.Credentials = nwc;

            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&StrFromDate={2}&CompanyId={3}", model.ReportName, model.ReportType, model.StrFromDate, model.CompanyId);

            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", model.ReportName + ".xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", model.ReportName + ".doc");
            }
            return View();
        }

        [HttpGet]

        public ActionResult SODLShareHolderPositionReport(ReportCustomModel model)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();

            client.Credentials = nwc;

            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&StrFromDate={2}&CompanyId={3}", model.ReportName, model.ReportType, model.StrFromDate, model.CompanyId);

            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", model.ReportName + ".xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", model.ReportName + ".doc");
            }
            return View();
        }


        [HttpGet]

        public ActionResult OPLShareHolderPositionReport(ReportCustomModel model)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();

            client.Credentials = nwc;

            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&StrFromDate={2}&CompanyId={3}", model.ReportName, model.ReportType, model.StrFromDate, model.CompanyId);

            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", model.ReportName + ".xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", model.ReportName + ".doc");
            }
            return View();
        }


        [HttpGet]

        public ActionResult KTTLShareHolderPositionReport(ReportCustomModel model)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();

            client.Credentials = nwc;

            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&StrFromDate={2}&CompanyId={3}", model.ReportName, model.ReportType, model.StrFromDate, model.CompanyId);

            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", model.ReportName + ".xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", model.ReportName + ".doc");
            }
            return View();
        }

        [HttpGet]

        public ActionResult KPLPoultryShareHolderPosition(int companyId, string reportName)
        {
            ReportCustomModel rcm = new ReportCustomModel()
            {
                CompanyId = companyId,
                FromDate = DateTime.Now,
                StrFromDate = DateTime.Now.ToShortDateString(),
                ReportName = reportName


            };
            return View(rcm);
        }


        [HttpGet]

        public ActionResult PoultryShareHolderPositionReport(ReportCustomModel model)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();

            client.Credentials = nwc;

            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&StrFromDate={2}&CompanyId={3}", model.ReportName, model.ReportType, model.StrFromDate, model.CompanyId);

            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", model.ReportName + ".xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", model.ReportName + ".doc");
            }
            return View();
        }

        [HttpGet]

        public ActionResult KGeCShareHolderPosition(int companyId, string reportName)
        {
            ReportCustomModel rcm = new ReportCustomModel()
            {
                CompanyId = companyId,
                FromDate = DateTime.Now,
                StrFromDate = DateTime.Now.ToShortDateString(),
                ReportName = reportName


            };
            return View(rcm);
        }


        [HttpGet]

        public ActionResult KGeCShareHolderPositionReport(ReportCustomModel model)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();

            client.Credentials = nwc;

            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&StrFromDate={2}&CompanyId={3}", model.ReportName, model.ReportType, model.StrFromDate, model.CompanyId);

            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", model.ReportName + ".xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", model.ReportName + ".doc");
            }
            return View();
        }

        [HttpGet]

        public ActionResult SaltShareHolderPosition(int companyId, string reportName)
        {
            ReportCustomModel rcm = new ReportCustomModel()
            {
                CompanyId = companyId,
                FromDate = DateTime.Now,
                StrFromDate = DateTime.Now.ToShortDateString(),
                ReportName = reportName


            };
            return View(rcm);
        }


        [HttpGet]

        public ActionResult SaltShareHolderPositionReport(ReportCustomModel model)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();

            client.Credentials = nwc;

            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&StrFromDate={2}&CompanyId={3}", model.ReportName, model.ReportType, model.StrFromDate, model.CompanyId);

            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", model.ReportName + ".xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", model.ReportName + ".doc");
            }
            return View();
        }


        [HttpGet]

        public ActionResult FisherisShareHolderPosition(int companyId, string reportName)
        {
            ReportCustomModel rcm = new ReportCustomModel()
            {
                CompanyId = companyId,
                FromDate = DateTime.Now,
                StrFromDate = DateTime.Now.ToShortDateString(),
                ReportName = reportName


            };
            return View(rcm);
        }


        [HttpGet]

        public ActionResult FisheriesShareHolderPositionReport(ReportCustomModel model)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();

            client.Credentials = nwc;

            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&StrFromDate={2}&CompanyId={3}", model.ReportName, model.ReportType, model.StrFromDate, model.CompanyId);

            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", model.ReportName + ".xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", model.ReportName + ".doc");
            }
            return View();
        }

        [HttpGet]

        public ActionResult FEEDShareHolderPosition(int companyId, string reportName)
        {
            ReportCustomModel rcm = new ReportCustomModel()
            {
                CompanyId = companyId,
                FromDate = DateTime.Now,
                StrFromDate = DateTime.Now.ToShortDateString(),
                ReportName = reportName
            };
            return View(rcm);
        }


        [HttpGet]

        public ActionResult FEEDShareHolderPositionReport(ReportCustomModel model)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();

            client.Credentials = nwc;

            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&StrFromDate={2}&CompanyId={3}", model.ReportName, model.ReportType, model.StrFromDate, model.CompanyId);

            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", model.ReportName + ".xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", model.ReportName + ".doc");
            }
            return View();
        }

        [HttpGet]

        public ActionResult PropertiesShareHolderPosition(int companyId, string reportName)
        {
            ReportCustomModel rcm = new ReportCustomModel()
            {
                CompanyId = companyId,
                FromDate = DateTime.Now,
                StrFromDate = DateTime.Now.ToShortDateString(),
                ReportName = reportName


            };
            return View(rcm);
        }


        [HttpGet]

        public ActionResult PropertiesShareHolderPositionReport(ReportCustomModel model)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();

            client.Credentials = nwc;

            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&StrFromDate={2}&CompanyId={3}", model.ReportName, model.ReportType, model.StrFromDate, model.CompanyId);

            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", model.ReportName + ".xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", model.ReportName + ".doc");
            }
            return View();
        }

        [HttpGet]

        public ActionResult SeedShareHolderPosition(int companyId, string reportName)
        {
            ReportCustomModel rcm = new ReportCustomModel()
            {
                CompanyId = companyId,
                FromDate = DateTime.Now,
                StrFromDate = DateTime.Now.ToShortDateString(),
                ReportName = reportName


            };
            return View(rcm);
        }


        [HttpGet]

        public ActionResult SeedShareHolderPositionReport(ReportCustomModel model)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();

            client.Credentials = nwc;

            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&StrFromDate={2}&CompanyId={3}", model.ReportName, model.ReportType, model.StrFromDate, model.CompanyId);

            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", model.ReportName + ".xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", model.ReportName + ".doc");
            }
            return View();
        }
        [HttpGet]

        public ActionResult TradingShareHolderPosition(int companyId, string reportName)
        {
            ReportCustomModel rcm = new ReportCustomModel()
            {
                CompanyId = companyId,
                FromDate = DateTime.Now,
                StrFromDate = DateTime.Now.ToShortDateString(),
                ReportName = reportName


            };
            return View(rcm);
        }


        [HttpGet]

        public ActionResult TradingShareHolderPositionReport(ReportCustomModel model)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();

            client.Credentials = nwc;

            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&StrFromDate={2}&CompanyId={3}", model.ReportName, model.ReportType, model.StrFromDate, model.CompanyId);

            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", model.ReportName + ".xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", model.ReportName + ".doc");
            }
            return View();
        }

        [HttpGet]

        public ActionResult FirmShareHolderPosition(int companyId, string reportName)
        {
            ReportCustomModel rcm = new ReportCustomModel()
            {
                CompanyId = companyId,
                FromDate = DateTime.Now,
                StrFromDate = DateTime.Now.ToShortDateString(),
                ReportName = reportName


            };
            return View(rcm);
        }


        [HttpGet]

        public ActionResult FirmShareHolderPositionReport(ReportCustomModel model)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();

            client.Credentials = nwc;

            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&StrFromDate={2}&CompanyId={3}", model.ReportName, model.ReportType, model.StrFromDate, model.CompanyId);

            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", model.ReportName + ".xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", model.ReportName + ".doc");
            }
            return View();
        }



        [HttpGet]

        public ActionResult ProfitLoss(int companyId, string reportName, string noteReportName)
        {
            //Session["CompanyId"] = companyId;
            ReportCustomModel cm = new ReportCustomModel()
            {
                CompanyId = companyId,
                FromDate = DateTime.Now,
                ToDate = DateTime.Now,
                StrFromDate = DateTime.Now.ToShortDateString(),
                StrToDate = DateTime.Now.ToShortDateString(),
                ReportName = reportName,
                NoteReportName = noteReportName,
                CostCenters = voucherTypeService.GetAccountingCostCenter(companyId)
            };
            return View(cm);
        }

        [HttpGet]

        public ActionResult GetProfitLossReport(ReportCustomModel model)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = "";
            long ReportCategoryId = 0;
            //if (model.CompanyId == 28 || model.CompanyId == 8 || model.CompanyId == 24 || model.CompanyId == 20 || model.CompanyId == 19)
            //{
            //    reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&CompanyId={2}&StrFromDate={3}&StrToDate={4}&CostCenterId={5}&ReportCategoryId={6}", model.ReportName, model.ReportType, model.CompanyId, model.StrFromDate, model.StrToDate, model.CostCenterId ?? 0, ReportCategoryId);
            //}
            //else
            //{
            //    reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&CompanyId={2}&StrFromDate={3}&StrToDate={4}&CostCenterId={5}", model.ReportName, model.ReportType, model.CompanyId, model.StrFromDate, model.StrToDate, model.CostCenterId ?? 0);
            //}
            reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&CompanyId={2}&StrFromDate={3}&StrToDate={4}&CostCenterId={5}&ReportCategoryId={6}", model.ReportName, model.ReportType, model.CompanyId, model.StrFromDate, model.StrToDate, model.CostCenterId ?? 0, ReportCategoryId);
            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", model.ReportName + ".xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", model.ReportName + ".doc");
            }
            return View();
        }



        [HttpGet]

        public ActionResult ProductionGcclReport(int companyId)
        {
            //Session["CompanyId"] = companyId;
            ReportCustomModel cm = new ReportCustomModel()
            {
                CompanyId = companyId,
                FromDate = DateTime.Now,
                ToDate = DateTime.Now,
                StrFromDate = DateTime.Now.ToShortDateString(),
                StrToDate = DateTime.Now.ToShortDateString(),

                CostCenters = voucherTypeService.GetAccountingCostCenter(companyId)
            };
            return View(cm);
        }

        [HttpGet]

        public ActionResult GcclProductionReportView(ReportCustomModel model)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            model.ReportName = "GCCLProductionReport";
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&StrFromDate={2}&StrToDate={3}&CompanyId={4}", model.ReportName, model.ReportType, model.StrFromDate, model.StrToDate, model.CompanyId);
            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", model.ReportName + ".xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", model.ReportName + ".doc");
            }
            return View();
        }


        [HttpGet]

        public ActionResult SeedSalesCollectionReport(int companyId)
        {
            //Session["CompanyId"] = companyId;
            ReportCustomModel cm = new ReportCustomModel()
            {
                CompanyId = companyId,
                FromDate = DateTime.Now,
                ToDate = DateTime.Now,
                StrFromDate = DateTime.Now.ToShortDateString(),
                StrToDate = DateTime.Now.ToShortDateString(),

                CostCenters = voucherTypeService.GetAccountingCostCenter(companyId)
            };
            return View(cm);
        }

        [HttpGet]

        public ActionResult SeedSalesCollectionView(ReportCustomModel model)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            model.ReportName = "SeedSalesCollectionStatement";
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&StrFromDate={2}&StrToDate={3}&CompanyId={4}", model.ReportName, model.ReportType, model.StrFromDate, model.StrToDate, model.CompanyId);
            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", model.ReportName + ".xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", model.ReportName + ".doc");
            }
            return View();
        }


        [HttpGet]

        public ActionResult ProfitLossService(int companyId, string reportName)
        {
            //Session["CompanyId"] = companyId;
            ReportCustomModel cm = new ReportCustomModel()
            {
                CompanyId = companyId,
                FromDate = DateTime.Now,
                ToDate = DateTime.Now,
                StrFromDate = DateTime.Now.ToShortDateString(),
                StrToDate = DateTime.Now.ToShortDateString(),
                ReportName = reportName,
                CostCenters = voucherTypeService.GetAccountingCostCenter(companyId)
            };
            return View(cm);
        }

        [HttpGet]

        public ActionResult ProfitLossServiceReport(ReportCustomModel model)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            long ReportCategoryId = 0;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&CompanyId={2}&StrFromDate={3}&StrToDate={4}&CostCenterId={5}&ReportCategoryId={6}", model.ReportName, model.ReportType, model.CompanyId, model.StrFromDate, model.StrToDate, model.CostCenterId ?? 0, ReportCategoryId);
            //string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&CompanyId={2}&StrFromDate={3}&StrToDate={4}&CostCenterId={5}", model.ReportName, model.ReportType, model.CompanyId, model.StrFromDate, model.StrToDate, model.CostCenterId ?? 0);

            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", model.ReportName + ".xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", model.ReportName + ".doc");
            }
            return View();
        }










        public ActionResult VoucherSearch(int companyId, string reportName)
        {
            //Session["CompanyId"] = companyId;
            ReportCustomModel cm = new ReportCustomModel()
            {
                CompanyId = companyId,
                FromDate = DateTime.Now,
                ToDate = DateTime.Now,
                StrFromDate = DateTime.Now.ToShortDateString(),
                StrToDate = DateTime.Now.ToShortDateString(),
                ReportName = reportName
            };
            return View(cm);
        }



        [HttpGet]

        public ActionResult GetVoucherSearchReport(ReportCustomModel model)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&CompanyId={2}&StrFromDate={3}&StrToDate={4}", model.ReportName, model.ReportType, model.CompanyId, model.StrFromDate, model.StrToDate);

            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", model.ReportName + ".xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", model.ReportName + ".doc");
            }
            return View();
        }




        public ActionResult VoucherTypeSearch(int companyId, string reportName)
        {
            ReportCustomModel cm = new ReportCustomModel()
            {
                CompanyId = companyId,
                VoucherTypes = voucherTypeService.GetVoucherTypeSelectModels(companyId),
                FromDate = DateTime.Now,
                ToDate = DateTime.Now,
                StrFromDate = DateTime.Now.ToShortDateString(),
                StrToDate = DateTime.Now.ToShortDateString(),
                ReportName = reportName
            };
            return View(cm);
        }

        [HttpGet]

        public ActionResult GetVoucherTypeSearchReport(ReportCustomModel model)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&CompanyId={2}&VoucherTypeId={3}&StrFromDate={4}&StrToDate={5}", model.ReportName, model.ReportType, model.CompanyId, model.VoucherTypeId, model.StrFromDate, model.StrToDate);

            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", model.ReportName + ".xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", model.ReportName + ".doc");
            }
            return View();
        }


        public ActionResult VoucherByVoucherNo(int companyId, string reportName)
        {
            //Session["CompanyId"] = companyId;
            ReportCustomModel cm = new ReportCustomModel()
            {
                CompanyId = companyId,
                ReportName = reportName
            };
            return View(cm);
        }

        [HttpGet]

        public ActionResult GetVoucherByVoucherNoReport(ReportCustomModel model)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&VoucherNo={2}&CompanyId={3}", model.ReportName, model.ReportType, model.VoucherNo, model.CompanyId);

            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", model.ReportName + ".xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", model.ReportName + ".doc");
            }
            return View();
        }

        // GET: Balance Sheet Report
        [HttpGet]

        public ActionResult TrailBalance(int companyId, string reportName)
        {
            //Session["CompanyId"] = companyId;
            var company = _accountingService.GetCompanyById(companyId);
            ReportCustomModel cm = new ReportCustomModel()
            {
                CompanyId = companyId,
                FromDate = DateTime.Now,
                ToDate = DateTime.Now,
                StrFromDate = DateTime.Now.ToShortDateString(),
                StrToDate = DateTime.Now.ToShortDateString(),

                CompanyName = company.Name

            };
            return View(cm);
        }

        [HttpGet]

        public ActionResult GetTrailBalanceReport(ReportCustomModel model)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            model.ReportName = "KGTrailBalance";
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&StrFromDate={2}&StrToDate={3}&CompanyId={4}&LayerNo={5}", model.ReportName, model.ReportType, model.StrFromDate, model.StrToDate, model.CompanyId, model.Id);

            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", model.ReportName + ".xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", model.ReportName + ".doc");
            }
            return View();
        }

        // GET: Customer Wise Monthly Sales Report
        [HttpGet]

        public ActionResult CustomerWiseMonthlySales(int companyId, string productType, string reportName, string title)
        {
            //Session["CompanyId"] = companyId;//Use to store CompanyId data into session to pass into report server url
            ReportCustomModel cm = new ReportCustomModel()
            {
                //CompanyId = companyId,
                //Years = companyService.GetSaleYearSelectModel(),
                //ProductType = productType,
                //ReportName = reportName,
                //Title = title
                Title = title,
                CompanyId = companyId,
                ProductType = productType,
                ReportName = reportName
            };
            return View(cm);
        }

        [HttpGet]

        public ActionResult GetCustomerWiseMonthlySalesReport(ReportCustomModel model)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&StrFromDate={2}&StrToDate={3}&CompanyId={4}&ProductType={5}&VendorId={6}", model.ReportName, model.ReportType, model.StrFromDate, model.StrToDate, model.CompanyId, model.ProductType, model.VendorId ?? 0);

            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", model.ReportName + ".xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", model.ReportName + ".doc");
            }
            return View();
        }

        // GET: Customer Wise Monthly Sales Report

        [HttpGet]

        public ActionResult GetCustomerWiseMonthlySaleYearBasisReport(ReportCustomModel model)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&CompanyId={2}&VendorId={3}&Year={4}&ProductType={5}", model.ReportName, model.ReportType, model.CompanyId, model.VendorId, model.Year, model.ProductType);

            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", model.ReportName + ".xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", model.ReportName + ".doc");
            }
            return View();
        }

        [HttpGet]

        public ActionResult GetFeedPurchaseReport(long storeId, int companyId, string reportName)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format=PDF&CompanyId={1}&StoreId={2}", reportName, companyId, storeId);
            return File(client.DownloadData(reportURL), "application/pdf");
        }


        public ActionResult SupplierWisePurchase(int companyId, string reportName, string title)
        {
            //Session["CompanyId"] = companyId;
            ReportCustomModel cm = new ReportCustomModel()
            {
                Title = title,
                CompanyId = companyId,
                ReportName = reportName
            };
            return View(cm);
        }

        [HttpGet]

        public ActionResult GetSupplierWisePurchaseSale(ReportCustomModel model)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&CompanyId={2}&VendorId={3}&StrFromDate={4}&StrToDate={5}", model.ReportName, model.ReportType, model.CompanyId, model.VendorId, model.StrFromDate, model.StrToDate);

            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", model.ReportName + ".xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", model.ReportName + ".doc");
            }
            return View();
        }

        // GET: Report
        [HttpGet]

        public ActionResult GetPurchaseReturnReport(string purchaseReturnId)
        {
            var companyId = Convert.ToInt32(Request.QueryString["companyId"]);
            string reportURL;
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            reportURL = url + "PurchaseReturn&rs:Command=Render&rs:Format=PDF&PurchaseReturnId=" + purchaseReturnId;
            return File(client.DownloadData(reportURL), "application/pdf");
        }

        //GET: Account Cheque Info

        [HttpGet]

        public ActionResult GetActChequeInfoReport(int companyId, int actChequeInfoId, string reportName)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format=PDF&CompanyId={1}&actChequeInfoId={2}", reportName, companyId, actChequeInfoId);
            return File(client.DownloadData(reportURL), "application/pdf");
        }

        //SeedReceiptPayment View
        [HttpGet]

        public ActionResult SeedReceiptPayment(int companyId)
        {
            //Session["CompanyId"] = companyId;
            ReportCustomModel cm = new ReportCustomModel()
            {
                CompanyId = companyId,
                Companies = companyService.GetCompanySelectModels(),
                FromDate = DateTime.Now,
                ToDate = DateTime.Now,
                StrFromDate = DateTime.Now.ToShortDateString(),
                StrToDate = DateTime.Now.ToShortDateString()
            };
            return View(cm);
        }
        [HttpGet]

        public ActionResult PackagingReceiptPayment(int companyId)
        {
            //Session["CompanyId"] = companyId;
            ReportCustomModel cm = new ReportCustomModel()
            {
                CompanyId = companyId,
                Companies = companyService.GetCompanySelectModels(),
                FromDate = DateTime.Now,
                ToDate = DateTime.Now,
                StrFromDate = DateTime.Now.ToShortDateString(),
                StrToDate = DateTime.Now.ToShortDateString()
            };
            return View(cm);
        }
        [HttpGet]

        public ActionResult PrintingReceiptPayment(int companyId)
        {
            //Session["CompanyId"] = companyId;
            ReportCustomModel cm = new ReportCustomModel()
            {
                CompanyId = companyId,
                Companies = companyService.GetCompanySelectModels(),
                FromDate = DateTime.Now,
                ToDate = DateTime.Now,
                StrFromDate = DateTime.Now.ToShortDateString(),
                StrToDate = DateTime.Now.ToShortDateString()
            };
            return View(cm);
        }
        [HttpGet]

        public ActionResult SODLReceiptPayment(int companyId)
        {
            //Session["CompanyId"] = companyId;
            ReportCustomModel cm = new ReportCustomModel()
            {
                CompanyId = companyId,
                Companies = companyService.GetCompanySelectModels(),
                FromDate = DateTime.Now,
                ToDate = DateTime.Now,
                StrFromDate = DateTime.Now.ToShortDateString(),
                StrToDate = DateTime.Now.ToShortDateString()
            };
            return View(cm);
        }
        [HttpGet]

        public ActionResult GLDLReceiptPayment(int companyId)
        {
            //Session["CompanyId"] = companyId;
            ReportCustomModel cm = new ReportCustomModel()
            {
                CompanyId = companyId,
                Companies = companyService.GetCompanySelectModels(),
                FromDate = DateTime.Now,
                ToDate = DateTime.Now,
                StrFromDate = DateTime.Now.ToShortDateString(),
                StrToDate = DateTime.Now.ToShortDateString()
            };
            return View(cm);
        }
        [HttpGet]

        public ActionResult GCCLReceiptPayment(int companyId)
        {
            //Session["CompanyId"] = companyId;
            ReportCustomModel cm = new ReportCustomModel()
            {
                CompanyId = companyId,
                Companies = companyService.GetCompanySelectModels(),
                FromDate = DateTime.Now,
                ToDate = DateTime.Now,
                StrFromDate = DateTime.Now.ToShortDateString(),
                StrToDate = DateTime.Now.ToShortDateString()
            };
            return View(cm);
        }
        [HttpGet]

        public ActionResult KFBLReceiptPayment(int companyId)
        {
            //Session["CompanyId"] = companyId;
            ReportCustomModel cm = new ReportCustomModel()
            {
                CompanyId = companyId,
                Companies = companyService.GetCompanySelectModels(),
                FromDate = DateTime.Now,
                ToDate = DateTime.Now,
                StrFromDate = DateTime.Now.ToShortDateString(),
                StrToDate = DateTime.Now.ToShortDateString()
            };
            return View(cm);
        }


        //GET: ReceiptPayment Statement Seed //SeedReceiptPaymentStatement

        [HttpGet]

        public ActionResult SeedReceiptPaymentReport(ReportCustomModel model)
        {
            var reportname = "";

            if (model.CompanyId == (int)CompanyName.KGECOM)
            {
                reportname = "KgComReceiptPaymentStatement";
            }
            else if (model.CompanyId == (int)CompanyName.KrishibidPackagingLimited)
            {
                reportname = "PackagingReceiptPaymentStatement";
            }
            else if (model.CompanyId == (int)CompanyName.KrishibidTradingLimited)
            {
                reportname = "TradingReceiptPaymentStatement";
            }
            else if (model.CompanyId == (int)CompanyName.SonaliOrganicDairyLimited)
            {
                reportname = "SODLReceiptPaymentStatement";
            }
            else if (model.CompanyId == (int)CompanyName.KrishibidPrintingAndPublicationLimited)
            {
                reportname = "PrintingReceiptPaymentStatement";
            }
            else if (model.CompanyId == (int)CompanyName.GloriousLandsAndDevelopmentsLimited)
            {
                reportname = "GLDLReceiptPaymentStatement";
            }
            else if (model.CompanyId == (int)CompanyName.GloriousCropCareLimited)
            {
                reportname = "GCCLReceiptPaymentStatement";
            }
            else if (model.CompanyId == (int)CompanyName.KrishibidFoodAndBeverageLimited)
            {




























































































































































































                reportname = "KFBLReceiptPaymentStatement";
            }
            else if (model.CompanyId == (int)CompanyName.KrishibidTradingLimited)
            {
                reportname = "TradingReceiptPaymentStatement";
            }
            else if (model.CompanyId == (int)CompanyName.KrishibidSeedLimited)
            {
                reportname = "SeedReceiptPaymentStatement";
            }
            //hello r2
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={4}&CompanyId={1}&StrFromDate={2}&StrToDate={3}", reportname, model.CompanyId, model.StrFromDate, model.StrToDate, model.ReportType);

            if (model.CompanyId == (int)CompanyName.KrishibidSeedLimited && model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", "ReceiptPaymentStatementSeed.xls");
            }
            else if (model.CompanyId == (int)CompanyName.KrishibidPackagingLimited && model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", "ReceiptPaymentStatementPackaging.xls");
            }
            else if (model.CompanyId == (int)CompanyName.KrishibidPackagingLimited && model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", "ReceiptPaymentStatementTrading.xls");
            }
            else if (model.CompanyId == (int)CompanyName.GloriousLandsAndDevelopmentsLimited && model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", "ReceiptPaymentStatementGLDL.xls");
            }
            else if (model.CompanyId == (int)CompanyName.SonaliOrganicDairyLimited && model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", "ReceiptPaymentStatementSODL.xls");
            }
            else if (model.CompanyId == (int)CompanyName.GloriousCropCareLimited && model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", "ReceiptPaymentStatementGCCL.xls");
            }
            else if (model.CompanyId == (int)CompanyName.KrishibidFoodAndBeverageLimited && model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", "ReceiptPaymentStatementKFBL.xls");
            }
            else if (model.CompanyId == (int)CompanyName.KrishibidPrintingAndPublicationLimited && model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", "ReceiptPaymentStatementKPPL.xls");
            }

            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }

            if (model.CompanyId == (int)CompanyName.KrishibidSeedLimited && model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", "ReceiptPaymentStatementSeed.doc");
            }
            else if (model.CompanyId == (int)CompanyName.KrishibidPackagingLimited && model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", "ReceiptPaymentStatementPackaging.doc");
            }
            else if (model.CompanyId == (int)CompanyName.KrishibidPackagingLimited && model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", "ReceiptPaymentStatementTrading.doc");
            }
            else if (model.CompanyId == (int)CompanyName.GloriousLandsAndDevelopmentsLimited && model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", "ReceiptPaymentStatementGLDL.doc");
            }
            else if (model.CompanyId == (int)CompanyName.SonaliOrganicDairyLimited && model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", "ReceiptPaymentStatementSODL.doc");
            }
            else if (model.CompanyId == (int)CompanyName.GloriousCropCareLimited && model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", "ReceiptPaymentStatementGCCL.doc");
            }
            else if (model.CompanyId == (int)CompanyName.KrishibidFoodAndBeverageLimited && model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", "ReceiptPaymentStatementKFBL.doc");
            }
            else if (model.CompanyId == (int)CompanyName.KrishibidPrintingAndPublicationLimited && model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", "ReceiptPaymentStatementKPPL.doc");
            }

            return View();
        }


        //kg3847-2022 start

        [HttpGet]

        public ActionResult DateWiseRawAttendance()
        {
            ReportCustomModel cm = new ReportCustomModel();
            cm.StrFromDate = DateTime.Now.ToShortDateString();
            return View(cm);
        }
        [HttpGet]

        public ActionResult DateWiseRawAttendanceReport(ReportCustomModel model)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            model.ReportName = "DateWiseRawAttendanceReport";
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&StrFromDate={2}", model.ReportName, model.ReportType, model.StrFromDate);

            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", "DateWiseRawAttendanceReport.xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", "DateWiseRawAttendanceReport.doc");
            }
            return View();
        }

        [HttpGet]

        public ActionResult IndividualAttendance(int companyId)
        {
            //Session["CompanyId"] = companyId;
            ReportCustomModel cm = new ReportCustomModel()
            {
                CompanyId = companyId,
                FromDate = DateTime.Now,
                ToDate = DateTime.Now,
                StrFromDate = DateTime.Now.ToShortDateString(),
                StrToDate = DateTime.Now.ToShortDateString(),
            };
            return View(cm);
        }

        [HttpGet]

        public ActionResult IndividualAttendanceSummaryReport(string employeeId, DateTime StrFromDate, DateTime StrToDate)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string ReportName = "EmployeeAttendanceReport";
            string ReportType = "PDF";
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&EmployeeId={2}&StrFromDate={3}&StrToDate={4}", ReportName, ReportType, employeeId, StrFromDate, StrToDate);
            if (ReportType == "PDF")
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            return View();
        }
        [HttpGet]

        public ActionResult IndividualAttendanceReport(ReportCustomModel model)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            model.ReportName = "EmployeeAttendanceReport";
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&EmployeeId={2}&StrFromDate={3}&StrToDate={4}", model.ReportName, model.ReportType, model.EmployeeKGId, model.StrFromDate, model.StrToDate);

            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", "BankCashBookSeed.xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", "GeneralLedger.doc");
            }
            return View();
        }




        [HttpGet]

        public ActionResult CompanyWiseSMSReport(int companyId, DateTime StrFromDate, DateTime StrToDate)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string ReportName = "CompanywiseSMSReport";
            string ReportType = "PDF";
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&CompanyId={2}&StrFromDate={3}&StrToDate={4}", ReportName, ReportType, companyId, StrFromDate, StrToDate);
            if (ReportType == "PDF")
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            return View();
        }

        [HttpGet]

        public ActionResult VoucherList(int companyId, string reportName)
        {
            //Session["CompanyId"] = companyId;
            ReportCustomModel cm = new ReportCustomModel()
            {
                VoucherTypesList = new SelectList(_accountingService.VoucherTypesDownList(companyId), "Value", "Text"),
                CompanyId = companyId,
                FromDate = DateTime.Now,
                ToDate = DateTime.Now,
                StrFromDate = DateTime.Now.ToShortDateString(),
                StrToDate = DateTime.Now.ToShortDateString(),
                ReportName = reportName
            };
            return View(cm);
        }
        [HttpGet]

        public ActionResult VoucherListReport(ReportCustomModel model)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&CompanyId={2}&StrFromDate={3}&StrToDate={4}&VoucherTypeId={5}", model.ReportName, model.ReportType, model.CompanyId, model.StrFromDate, model.StrToDate, model.VmVoucherTypeId);

            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", "BankCashBookSeed.xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", "GeneralLedger.doc");
            }
            return View();
        }


        [HttpGet]

        public ActionResult KSSLShareHolderPosition(int companyId)
        {
            ReportCustomModel rcm = new ReportCustomModel()
            {
                CompanyId = companyId,
                FromDate = DateTime.Now,
                StrFromDate = DateTime.Now.ToShortDateString(),
            };
            return View(rcm);
        }

        [HttpGet]

        public ActionResult KSSLShareHolderPositionReport(ReportCustomModel model)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            model.ReportName = "KSSLShareholderPosition";
            client.Credentials = nwc;

            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&StrFromDate={2}&CompanyId={3}", model.ReportName, model.ReportType, model.StrFromDate, model.CompanyId);

            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", model.ReportName + ".xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", model.ReportName + ".doc");
            }
            return View();
        }


        [HttpGet]

        public ActionResult CollectionExpenditureStatements(int companyId)
        {
            ReportCustomModel rcm = new ReportCustomModel()
            {
                CompanyId = companyId,
                FromDate = DateTime.Now,
                StrFromDate = DateTime.Now.ToShortDateString(),
            };
            return View(rcm);
        }

        [HttpPost]

        public ActionResult CollectionExpenditureStatements(ReportCustomModel model)
        {
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            model.ReportName = "CollectionExpenditureStatements";
            client.Credentials = nwc;

            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format=PDF&CompanyId={1}&StrFromDate={2}&StrToDate={3}", model.ReportName, model.CompanyId, model.StrFromDate, model.StrToDate);
            if (model.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", model.ReportName + ".xls");
            }
            if (model.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            if (model.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", model.ReportName + ".doc");
            }
            return View();
        }








        private List<SelectMarketingModel> hours()
        {
            List<SelectMarketingModel> hours = new List<SelectMarketingModel>();
            hours.Add(new SelectMarketingModel { Text = "12 AM", Value = 0 });
            hours.Add(new SelectMarketingModel { Text = "1 AM", Value = 1 });
            hours.Add(new SelectMarketingModel { Text = "2 AM", Value = 2 });
            hours.Add(new SelectMarketingModel { Text = "3 AM", Value = 3 });
            hours.Add(new SelectMarketingModel { Text = "4 AM", Value = 4 });
            hours.Add(new SelectMarketingModel { Text = "5 AM", Value = 5 });
            hours.Add(new SelectMarketingModel { Text = "6 AM", Value = 6 });
            hours.Add(new SelectMarketingModel { Text = "7 AM", Value = 7 });
            hours.Add(new SelectMarketingModel { Text = "8 AM", Value = 8 });
            hours.Add(new SelectMarketingModel { Text = "9 AM", Value = 9 });
            hours.Add(new SelectMarketingModel { Text = "10 AM", Value = 10 });
            hours.Add(new SelectMarketingModel { Text = "11 AM", Value = 11 });
            hours.Add(new SelectMarketingModel { Text = "12 PM", Value = 12 });
            hours.Add(new SelectMarketingModel { Text = "1 PM", Value = 13 });
            hours.Add(new SelectMarketingModel { Text = "2 PM", Value = 14 });
            hours.Add(new SelectMarketingModel { Text = "3 PM", Value = 15 });
            hours.Add(new SelectMarketingModel { Text = "4 PM", Value = 16 });
            hours.Add(new SelectMarketingModel { Text = "5 PM", Value = 17 });
            hours.Add(new SelectMarketingModel { Text = "6 PM", Value = 18 });
            hours.Add(new SelectMarketingModel { Text = "7 PM", Value = 19 });
            hours.Add(new SelectMarketingModel { Text = "8 PM", Value = 20 });
            hours.Add(new SelectMarketingModel { Text = "9 PM", Value = 21 });
            hours.Add(new SelectMarketingModel { Text = "10 PM", Value = 22 });
            hours.Add(new SelectMarketingModel { Text = "11 PM", Value = 23 });
            var List = new List<object>();

            //for (int i = 0; i < 13; i++)
            //{
            //    List.Add(new
            //    {
            //        Value = i,
            //        Text = i.ToString()
            //    });
            //}
            return hours;
        }
        private List<object> minites()
        {
            var List = new List<object>();
            for (int i = 0; i < 65; i = i + 5)
            {
                List.Add(new
                {
                    Value = i,
                    Text = i.ToString()
                });
            }
            return List;
        }




        [HttpGet]

        public ActionResult staticdataForGeneral(int companyid, int AccHeadId)
        {
            ReportCustomModel cm = new ReportCustomModel();
            cm.ReportType = "PDF";
            cm.StrToDate = DateTime.Now.ToString();
            string reportName = "";
            reportName = "KGGeneralLedger";
            NetworkCredential nwc = new NetworkCredential(admin, password);
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format(url + "{0}&rs:Command=Render&rs:Format={1}&AccHeadId={2}&StrFromDate={3}&StrToDate={4}&CompanyId={5}", reportName, cm.ReportType, AccHeadId, "11/07/1995", cm.StrToDate, companyid);

            if (cm.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");
            }
            return View();
        }


    }
}
﻿using DocumentFormat.OpenXml.EMMA;
using KGERP.Service.Configuration;
using KGERP.Service.Implementation.Audit;
using KGERP.Service.Implementation.Audit.ViewModels;

using KGERP.Service.ServiceModel;

using KGERP.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Services.Description;

namespace KGERP.Controllers.Audit
{
    public class AuditController : Controller
    {
        
        private IAuditService _auditService;
       
        public AuditController(IAuditService auditService)
        {
            _auditService = auditService;
           
        }
        // GET: Audit
        [HttpGet]
        public ActionResult Index(int? companyId, int? year,int? month,int? type, int companyId2 = 0)
        {
            PreservingAuditDocumentVM model = new PreservingAuditDocumentVM();
            var data = _auditService.GetAllAuditDocument(companyId, year, month, type, companyId2);

            if (data != null)
            {
                model.DataList = data.ToList();
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult ListIndex2(int? companyId, int? year, int? month, int? type, int companyId2 = 0)
        {
            PreservingAuditDocumentVM model = new PreservingAuditDocumentVM();
            var data = _auditService.GetAllAuditDocument(companyId, year, month, type, companyId2);

            if (data != null)
            {
                model.DataList = data.ToList();
            }
            return View(model);
        }

        [HttpPost]
        [SessionExpire]
        public async Task<ActionResult> Index(PreservingAuditDocumentVM model)
        {
            if (model.CompanyId > 0)
            {
                Session["CompanyId"] = model.CompanyId;
            }

            model.FromDate = Convert.ToDateTime(model.StrFromDate);
            model.ToDate = Convert.ToDateTime(model.StrToDate);


            return RedirectToAction(nameof(Index), new { companyId = 0, Year = model.Year, Month = model.Month, Type =(int)model.Type, companyId2 = model.CompanyId });

            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> FileUpload(int Id)
        {
            PreservingAuditDocumentVM model = new PreservingAuditDocumentVM();
            model = await Task.Run(() => _auditService.GetAuditDocumentFileAsync(Id));
            return View(model);
        }
        [HttpGet]
        public async Task<ActionResult> FileUpload2(int Id)
        {
            PreservingAuditDocumentVM model = new PreservingAuditDocumentVM();
            model = await Task.Run(() => _auditService.GetAuditDocumentFileAsync(Id));
            return View(model);
        }



        //[HttpGet]
        //public ActionResult LoadAuditDocument(DateTime? fromDate, DateTime? toDate,int? type,int CompanyId)
        //{
        //    PreservingAuditDocumentVM model = new PreservingAuditDocumentVM();
        //    var data = _auditService.GetAllAuditDocument(fromDate,toDate,type);
        //    if (data != null && data.Count() > 0)
        //    {
        //        model.DataList = data.ToList();
        //    }
        //    return View("Index", model);
        //}


   




        [HttpGet]
        public ActionResult AddOrUpdateAuditDocument(int? id)
        {

            if (id != null)
            {
                var modelVM = _auditService.GetAuditDocumentById(id.Value);
                if (modelVM != null)
                {
                    return View(modelVM);
                }
            }
            PreservingAuditDocumentVM model = new PreservingAuditDocumentVM();
            var data = _auditService.GetAllAuditDocument(null,null,null,null);
            if (data != null)
            {
                model.DataList = data.ToList();
            }
            return View(model);
        }
        [HttpPost]
        public ActionResult AddOrUpdateAuditDocument(PreservingAuditDocumentVM modelVM)
        {
            if (ModelState.IsValid)
            {
                if (modelVM.ActionEnum == Utility.ActionEnum.Add)
                {

                    var result = _auditService.AddAuditDocument(modelVM);
                }
                if (modelVM.ActionEnum == Utility.ActionEnum.Edit)
                {

                    var result = _auditService.UpdateAuditDocument(modelVM);
                }
                if (modelVM.ActionEnum == Utility.ActionEnum.Delete)
                {
                    var result = _auditService.DeleteAuditDocument(modelVM.Id);
                }
            }
            return RedirectToAction("FileUpload", new { Id = modelVM.Id });
        }

        [HttpPost]
        public ActionResult RemoveAuditDocument(long id)
        {
            var result = _auditService.DeleteAuditDocument(id);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult GetAuditDocumentById(long id)
        {
            var data = _auditService.GetAuditDocumentById(id);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}
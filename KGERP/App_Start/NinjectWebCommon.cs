using KGERP.App_Start;
using KGERP.Data.Models;
using KGERP.Service.Implementation;
using KGERP.Service.Implementation.ApprovalSystemService;
using KGERP.Service.Implementation.Audit;
using KGERP.Service.Implementation.AutoComplete;
using KGERP.Service.Implementation.General_Requisition;
using KGERP.Service.Interface;
using KGERP.Service.ServiceModel;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Ninject;
using Ninject.Web.Common;
using Ninject.Web.Common.WebHost;
using System;
using System.Configuration;
using System.Data.Entity;
using System.Web;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(NinjectWebCommon), "Stop")]

namespace KGERP.App_Start
{
    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
            kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

            RegisterServices(kernel);
            return kernel;
        }

        private static void RegisterServices(IKernel kernel)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["ERPEntities"].ConnectionString;
            kernel.Bind<DbContext>().To<ERPEntities>().InRequestScope().WithConstructorArgument("connectionString", connectionString);
            kernel.Bind<IDropDownTypeService>().To<DropDownTypeService>().InRequestScope();
         
            kernel.Bind<ICompanyService>().To<CompanyService>().InRequestScope();
            kernel.Bind<ICompanyMenuService>().To<CompanyMenuService>().InRequestScope();
            kernel.Bind<ICompanySubMenuService>().To<CompanySubMenuService>().InRequestScope();
            kernel.Bind<ICompanyUserMenuService>().To<CompanyUserMenuService>().InRequestScope();
           
            kernel.Bind<IAccountHeadService>().To<AccountHeadService>().InRequestScope();
           
            kernel.Bind<IVoucherService>().To<VoucherService>().InRequestScope();
          
            kernel.Bind<IVoucherTypeService>().To<VoucherTypeService>().InRequestScope();
           
            kernel.Bind<IAccountSignatoryService>().To<AccountSignatoryService>().InRequestScope();
           
            kernel.Bind<IApproval_Service>().To<Approval_Service>().InRequestScope();           
            
            kernel.Bind<IAutoCompleteService>().To<AutoCompleteService>().InRequestScope();
            kernel.Bind<IHeadGLService>().To<HeadGLService>().InRequestScope();
           
          
            

        }
    }
}